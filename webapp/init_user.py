import json
import os

from shipka.store import database
from webapp.api.models import UserApp, UserView, ProddayApp, Component, Settings, UserComponent


def create_apps_and_components():
    # walk over apps directory
    apps_dir = os.path.join('apps')
    for _, dirs, _ in os.walk(apps_dir):
        for dir in dirs:
            app_dir = os.path.join(apps_dir, dir)
            manifest = json.load(open(os.path.join(app_dir, 'manifest.json')))
            # create the app
            app = ProddayApp.create(
                name=manifest['app_name'],
                description=manifest['description'],
                developer=manifest['developer'],
                icon=manifest['icon'],
            )
            # iterate through the components
            for component_d in manifest['components']:
                t = component_d.get('type', 'list')
                Component.create(
                    name=component_d['name'],
                    collection=component_d['collection'],
                    type=t,
                    app=app,
                )
        break
    database.push()


def init_user(user):
    # add app settings view
    app_settings = ProddayApp.get_or_create(name="Settings")
    UserApp.get_or_create(app=app_settings, user=user, enabled=True)
    user_view_settings = UserView.get_or_create(user=user, name='settings', icon='cogs', order=99)

    # add USER_VIEW SETTINGS component to the view
    user_view_component = Component.get_or_create(name='user_view_settings', app=app_settings)
    UserComponent.get_or_create(component=user_view_component,
                                user_view=user_view_settings,
                                user=user,
                                order=1)

    # add PRODDAY APPS component to the view
    app_list_component = Component.get_or_create(name="prodday_app_list", app=app_settings)
    UserComponent.get_or_create(component=app_list_component,
                                user_view=user_view_settings,
                                user=user,
                                order=2)

    # add TRACKER LIST component to the view
    tracker_list_component = Component.get_or_create(name="tracker_list", app=app_settings)
    UserComponent.get_or_create(component=tracker_list_component,
                                user_view=user_view_settings,
                                user=user,
                                order=3)
    Settings.get_or_create(user=user)
    database.push()
    return [
        user_view_settings,
    ]


def _activities_view(user):
    # add app activities view
    app_acitivities = ProddayApp.get_or_create(name="Activities")
    UserApp.get_or_create(app=app_acitivities, user=user, enabled=True)
    user_view_activities = UserView.get_or_create(user=user, name='activities', icon='child', order=1)

    # add CREATE ACTIVITY component to the view
    activity_create_component = Component.get_or_create(name="activity_create", app=app_acitivities)
    activity_create_user_component = UserComponent.get_or_create(component=activity_create_component,
                                                                 user_view=user_view_activities,
                                                                 user=user,
                                                                 columns=12,
                                                                 order=1)

    # add RUNNING ACTIVITIES component to the view
    activity_list_component = Component.get_or_create(name="activity_list", app=app_acitivities)
    activity_list_user_component = UserComponent.get_or_create(component=activity_list_component,
                                                               user_view=user_view_activities,
                                                               user=user,
                                                               columns=12,
                                                               order=2)
    activity_list_user_component.options = {'is_running': True}

    # add ACTIVITY SESSION LIST component to the view
    activity_session_list_component = Component.get_or_create(name="activity_session_list", app=app_acitivities)
    UserComponent.get_or_create(component=activity_session_list_component,
                                user_view=user_view_activities,
                                user=user,
                                columns=6,
                                order=3)

    # add NOT RUNNING ACTIVITIES component to the view
    activity_list_not_running_component = UserComponent.get_or_create(component=activity_list_component,
                                                                      user_view=user_view_activities,
                                                                      user=user,
                                                                      columns=6,
                                                                      order=4)
    activity_list_not_running_component.options = {'is_running': False}
    return user_view_activities


def _goals_view(user):
    # add app activities view
    app_goals = ProddayApp.get_or_create(name="Goals")
    UserApp.get_or_create(app=app_goals, user=user, enabled=True)
    user_view_goals = UserView.get_or_create(user=user, name='goals', icon='space-shuttle', order=2)

    # add GROUP GOAL USERS component to the view
    goal_list_component = Component.get_or_create(name="goal_list", app=app_goals)
    UserComponent.get_or_create(component=goal_list_component,
                                user_view=user_view_goals,
                                user=user,
                                columns=12,
                                order=1)
    return user_view_goals


def init_goal_user(user):
    user_view_activities = _activities_view(user)
    user_view_goals = _goals_view(user)

    # create initial settings
    Settings.get_or_create(user=user)
    database.push()
    return [
        user_view_activities,
        user_view_goals,
    ]
