COMPONENT_REGISTRY = {}


def build_dependencies(model_scripts, view_components):
    view = {
        'components': [],
        'templates': [],
        'scripts': [],
        'external_scripts': [],
        'styles': [],
    }
    for view_component in view_components:
        component = COMPONENT_REGISTRY.get(view_component.component.name)
        component_name = component['name']
        app_name = component['app_name']
        view['components'].append(view_component)
        for component_key in ['templates', 'scripts', 'styles', 'external_scripts']:
            if component_key in component:
                for template in component[component_key]:
                    if (app_name, component_name, template) not in view[component_key]:
                        view[component_key].append((app_name, component_name, template))
    return model_scripts, view
