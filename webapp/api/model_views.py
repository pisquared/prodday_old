import datetime
from datetime import timedelta
from flask import request, jsonify
from flask_security import login_user

from models import Task, CalendarEvent, Activity, ActivitySession, Goal, GoalCompleteness, InboxEntry, Note, \
    Notification, DesktopAppTrackerSession, DomainTrackerSession, Tracker, Component, UserComponent, UserView, \
    GroupGoal, GroupGoalUserSettings, GroupGoalPost, GroupGoalPostReaction, GroupGoalPostComment, \
    CalendarEventNotification, ProddayApp, UserApp
from shipka.store.database import push as db_push, add as db_add, User
from shipka.webapp import csrf
from shipka.webapp.registries import MODELS_REGISTRY
from shipka.webapp.util import get_now_user, get_user, parse_dt, get_beginning_of_week_user, get_today_user
from webapp.api import api
from webapp.api.common import ProddayModelView, SinceModelView


class TaskModelView(ProddayModelView):
    def before_create(self, form_data):
        user = get_user()
        now = get_now_user(user)
        start_ts = form_data.get('start_ts')
        if not start_ts:
            form_data['start_ts'] = now
        return form_data

    def _sort_and_format(self, tasks):
        return sorted(tasks, key=lambda t: t.created_ts, reverse=True)

    def _get_start_ts_end_of_day(self, start_ts=None, end_ts=None, user=None):
        start_ts = start_ts or get_now_user(user)
        start_of_the_day = datetime.datetime(year=start_ts.year, month=start_ts.month, day=start_ts.day)
        return start_of_the_day.replace(hour=23, minute=59, second=59)

    def retrieve_not_started(self, start_ts=None, end_ts=None):
        user = get_user()
        _start_ts_end_of_day = self._get_start_ts_end_of_day(start_ts=start_ts, user=user)
        tasks = self.model.query.filter(Task.user_id == user.id,
                                        Task.deleted == False,
                                        Task.status == Task.STATUS_NOT_STARTED,
                                        Task.start_ts <= _start_ts_end_of_day).all()
        return self._sort_and_format(tasks)

    def retrieve_done(self, start_ts=None, end_ts=None):
        user = get_user()
        _start_ts_end_of_day = self._get_start_ts_end_of_day(start_ts=start_ts, user=user)
        tasks = self.model.query.filter(Task.user_id == user.id,
                                        Task.deleted == False,
                                        Task.status == Task.STATUS_DONE,
                                        Task.start_ts <= _start_ts_end_of_day).all()
        return self._sort_and_format(tasks)

    def retrieve_all(self, start_ts=None, end_ts=None):
        user = get_user()
        _start_ts_end_of_day = self._get_start_ts_end_of_day(start_ts=start_ts, user=user)
        tasks = self.model.query.filter(Task.user_id == user.id,
                                        Task.deleted == False,
                                        Task.start_ts <= _start_ts_end_of_day).all()
        return self._sort_and_format(tasks)

    def handle_get_filters(self, filters):
        retrieve_filters = dict()
        if 'start_ts' in filters:
            retrieve_filters['start_ts'] = parse_dt(filters['start_ts'])
        if 'end_ts' in filters:
            retrieve_filters['end_ts'] = parse_dt(filters['end_ts'])
        if 'is_done' in filters:
            if filters['is_done'] == '1':
                return self.retrieve_done(**retrieve_filters)
            elif filters['is_done'] == '0':
                return self.retrieve_not_started(**retrieve_filters)
        return self.retrieve_all(**retrieve_filters)

    def handle_patch(self, instance, update):
        rv = super(TaskModelView, self).handle_patch(instance, update)
        user = get_user()
        now = get_now_user(user)
        if 'status' in update:
            status = update.pop('status')
            if status in [Task.STATUS_NOT_STARTED, Task.STATUS_IN_PROGRESS, Task.STATUS_DONE, ]:
                rv['status'] = status
                if status == Task.STATUS_DONE:
                    rv['done_ts'] = now
                elif status == Task.STATUS_IN_PROGRESS:
                    rv['in_progress_ts'] = now
        return rv


class CalendarEventModelView(ProddayModelView):
    def before_create(self, form_data):
        user = get_user()
        now = get_now_user(user)
        start_ts = form_data.get('start_ts')
        end_ts = form_data.get('end_ts')

        if not start_ts:
            form_data['start_ts'] = now
        if not end_ts:
            form_data['end_ts'] = now + timedelta(hours=1)
        return form_data

    def after_create(self, serialized_instance):
        """Create a notification"""
        # unpack instance
        serialized_instance = serialized_instance.get('instance')
        notifications = serialized_instance.get('notifications')
        if not notifications:
            # create default notification
            instance_id = serialized_instance.get('id')
            start_ts = parse_dt(serialized_instance.get('start_ts'))
            notification_data = dict(message_template='calendar_event',
                                     message_context={
                                         'model': 'CalendarEvent',
                                         'instance_id': instance_id
                                     },
                                     url='/calendar_events/{}/edit'.format(instance_id),
                                     schedule_ts=start_ts + timedelta(minutes=1))
            notification_serialized = MODELS_REGISTRY.get('notification').get('vm').add_create(notification_data)
            CalendarEventNotification.create(
                calendar_event_id=instance_id,
                notification=notification_serialized.get('id')
            )
            db_push()
            serialized_instance['notifications'] = [
                notification_serialized.get('instance')
            ]
        return serialized_instance

    def handle_get_filters(self, filters):
        # TODO: test with /api/calendar_events?start=2018-01-29&end=2018-02-02&event_type=recurring
        user = get_user()
        if 'day' in filters:
            day = filters['day']
            if day == 'today':
                return self.model.get_events_today(user=user)
            elif day == 'normal_today':
                return self.model.get_normal_events_today(user=user)
            elif day == 'recurring_today':
                return self.model.get_recurring_events_today(user=user)
        if 'start' in filters:
            if 'end' not in filters:
                return []
            start = filters['start']
            end = filters['end']
            start_ts = parse_dt(start)
            end_ts = parse_dt(end)
            if 'event_type' not in filters:
                return self.model.get_events_in_bounds(user=user, start=start_ts, end=end_ts)
            else:
                event_type = filters['event_type']
                if event_type == 'normal':
                    return self.model.get_normal_events_in_bounds(user=user, start=start_ts, end=end_ts)
                elif event_type == 'recurring':
                    return self.model.get_recurring_events_in_bounds(user=user, start=start_ts, end=end_ts)
        return self.model.get_all_by(user=user)


class ActivityModelView(ProddayModelView):
    def activity_start(self, instance, user, now, start_type):
        running_activity_session = ActivitySession.get_one_by(activity=instance,
                                                              user=user,
                                                              status=ActivitySession.STATUS_IN_PROGRESS)
        if running_activity_session:
            raise Exception("Activity already running. Please stop it before continuing.")
        activity_session_vm = MODELS_REGISTRY.get('activity_session').get('vm')
        activity_rv = activity_session_vm.add_create(dict(start_ts=now,
                                                          start_type=start_type,
                                                          activity=instance))
        return activity_rv['instance']

    def activity_stop(self, instance, user, now):
        running_activity_session = ActivitySession.get_one_by(activity=instance,
                                                              user=user,
                                                              status=ActivitySession.STATUS_IN_PROGRESS)
        if not running_activity_session:
            raise Exception("Activity not started.")
        updated_elements = {'end_ts': now,
                            'status': ActivitySession.STATUS_DONE}
        activity_sessions_vm = MODELS_REGISTRY.get('activity_session').get('vm')
        activity_sessions_vm.add_update(running_activity_session, updated_elements)
        return running_activity_session

    def handle_patch(self, instance, update):
        rv = super(ActivityModelView, self).handle_patch(instance, update)
        start_type = update.get('start_type')
        if start_type:
            del update['start_type']
        else:
            start_type = ActivitySession.START_TYPE_MANUAL
        if 'is_running' in update:
            is_running = update.pop('is_running')
            if is_running in [True, False]:
                user = get_user()
                now = get_now_user(user)
                rv['is_running'] = is_running
                if is_running:
                    rv['start_ts'] = now
                    activity_session = self.activity_start(instance, user, now, start_type=start_type)
                    rv['running_activity_session_id'] = activity_session['id']
                else:
                    rv['start_ts'] = None
                    self.activity_stop(instance, user, now)
                    rv['running_activity_session_id'] = None
                db_push()
        return rv


class ActivitySessionModelView(ProddayModelView):
    def retrieve(self):
        # TODO: respect user/group permissions,
        rv = []
        user = get_user()
        user_serialized = user.serialize_flat()
        activiy_sessions = ActivitySession.get_all_by(user=user, deleted=False)
        for activiy_session in activiy_sessions:
            activiy_session_serialized = activiy_session.serialize_flat()
            if activiy_session.activity:
                activiy_session_serialized['activity'] = activiy_session.activity.serialize_flat()
            else:
                activiy_session_serialized['activity'] = {'name': ''}
            activiy_session_serialized['user'] = user_serialized
            rv.append(activiy_session_serialized)
        return jsonify(rv)


class InboxEntryModelView(ProddayModelView):
    def before_create(self, form_data):
        body = form_data.get('body')
        if body.lower().startswith('todo:'):
            task_vm = MODELS_REGISTRY.get('task').get('vm')
            body = body[len('todo:'):].strip()
            response = task_vm.add_create({'title': body})
            task_id = response['instance']['id']
            form_data['task_id'] = task_id
        elif body.lower().startswith('event:'):
            calendar_event_vm = MODELS_REGISTRY.get('calendar_event').get('vm')
            body = body[len('event:'):].strip()
            response = calendar_event_vm.add_create({'title': body})
            calendar_event_id = response['instance']['id']
            form_data['calendar_event_id'] = calendar_event_id
        elif body.lower().startswith('start:'):
            from webapp.api.models import Activity, ActivitySession
            activities_vm = MODELS_REGISTRY.get('activity').get('vm')
            activity_name = body[len('start:'):].strip()
            activity = Activity.get_one_by(name=activity_name)
            if not activity:
                return form_data
            user = get_user()
            now = get_now_user(user)
            updated_elements = {
                "is_running": True,
                "start_ts": now,
                "start_type": ActivitySession.START_TYPE_MANUAL,
            }
            response = activities_vm.handle_patch(activity, update=updated_elements)
            activities_vm.add_update(activity, response)
            form_data['activity_session_id'] = response['running_activity_session_id']
        elif body.lower().startswith('stop:'):
            from webapp.api.models import Activity, ActivitySession
            activities_vm = MODELS_REGISTRY.get('activity').get('vm')
            activity_name = body[len('stop:'):].strip()
            activity = Activity.get_one_by(name=activity_name)
            if not activity:
                return form_data
            user = get_user()
            now = get_now_user(user)
            updated_elements = {
                "is_running": False,
                "start_ts": None,
                "start_type": ActivitySession.START_TYPE_MANUAL,
            }
            response = activities_vm.handle_patch(activity, update=updated_elements)
            activities_vm.add_update(activity, response)
            form_data['activity_session_id'] = response['running_activity_session_id']
        return form_data


class TrackerSessionModelView(SinceModelView):
    LIMIT_BATCH_CREATE_TRACKER_SESSIONS = 50

    @csrf.exempt
    def batch_create(self):
        form_data = request.form or request.json
        tracker_id = form_data.get('tracker_id')
        tracker = Tracker.get_one_by(id=tracker_id)
        if not tracker:
            raise Exception('No tracker with id {}'.format(tracker_id))
        api_key = form_data.get('api_key')
        if not tracker.api_key == api_key:
            raise Exception('Wrong API key for tracker with id {}'.format(tracker_id))
        login_user(tracker.user)
        return super(TrackerSessionModelView, self).batch_create()

    def serialize_batch_create(self):
        form_data = request.form or request.json
        sessions = form_data.get('sessions')
        if len(sessions) > self.LIMIT_BATCH_CREATE_TRACKER_SESSIONS:
            raise Exception(
                'Attempted to create too many sessions. Limit: {}, Got: {}'.format(
                    self.LIMIT_BATCH_CREATE_TRACKER_SESSIONS,
                    len(sessions)))
        rv = []
        for session in sessions:
            session['tracker_id'] = form_data.get('tracker_id')
            rv.append(session)
        return rv

    def after_batch_create(self, responses):
        # start the process_stats task on the queue
        user = get_user()
        from workers.tasks import process_stats
        process_stats.delay(user_id=user.id)
        return responses


class GoalModelView(ProddayModelView):
    def retrieve(self):
        # TODO: respect user/group permissions,
        rv = []
        user = get_user()
        user_serialized = user.serialize_flat()
        goals = Goal.get_all_by(user=user, deleted=False)
        for goal in goals:
            if goal.period == Goal.PERIOD_WEEK:
                start_ts = get_beginning_of_week_user(user=goal.user)
            else:
                start_ts = get_today_user(user=goal.user)
            goal_completeness = GoalCompleteness.get_one_by(goal=goal,
                                                            start_ts=start_ts)
            goal_serialized = goal.serialize_flat()
            if goal_completeness:
                goal_serialized['goal_completeness'] = goal_completeness.serialize_flat()
            else:
                goal_serialized['goal_completeness'] = {'total_minutes': 0,
                                                        'value': 0}
            if goal.activity:
                goal_serialized['activity'] = goal.activity.serialize_flat()
            else:
                goal_serialized['activity'] = {'icon': ''}
            goal_serialized['user'] = user_serialized
            rv.append(goal_serialized)
        return jsonify(rv)


class ProddayAppModelView(ProddayModelView):
    def retrieve(self):
        rv = []
        user = get_user()
        prodday_apps = ProddayApp.get_all()
        for prodday_app in prodday_apps:
            app_serialized = prodday_app.serialize_flat()
            user_app = UserApp.get_one_by(app_id=prodday_app.id,
                                          user=user)
            if user_app:
                app_serialized['user_app'] = user_app.serialize_flat()
            else:
                app_serialized['user_app'] = {
                    'enabled': False
                }
            components = []
            for component in prodday_app.components:
                components.append(component.serialize_flat())
            app_serialized['components'] = components
            rv.append(app_serialized)
        return jsonify(rv)


class UserComponentModelView(ProddayModelView):
    def retrieve(self):
        rv = []
        user = get_user()
        user_components = UserComponent.get_all_by(user=user)
        for user_component in user_components:
            user_component_serialized = user_component.serialize_flat()
            component = Component.get_one_by(id=user_component.component_id)
            user_component_serialized['component'] = component.serialize_flat()
            rv.append(user_component_serialized)
        return jsonify(rv)


class ComponentModelView(ProddayModelView):
    def retrieve(self):
        rv = []
        components = Component.get_all()
        for component in components:
            component_serialized = component.serialize_flat()
            app = ProddayApp.get_one_by(id=component.app_id)
            user_app = UserApp.get_one_by(app_id=app.id)
            component_serialized['app'] = app.serialize_flat()
            if user_app:
                component_serialized['user_app'] = user_app.serialize_flat()
            else:
                component_serialized['user_app'] = {
                    'enabled': False
                }
            rv.append(component_serialized)
        return jsonify(rv)


class GroupGoalModelView(ProddayModelView):
    def retrieve_single(self, instance_id):
        from shipka.webapp import gravatar
        # TODO: respect user/group permissions,
        group_goal = GroupGoal.get_one_by(id=instance_id)
        gg = group_goal.serialize_flat()
        gg_users = GroupGoalUserSettings.get_all_by(group_goal=group_goal)
        gg['users'] = []
        for gg_user in gg_users:
            goal = Goal.get_one_by(id=gg_user.goal_id)
            if goal.period == Goal.PERIOD_WEEK:
                start_ts = get_beginning_of_week_user(user=goal.user)
            else:
                start_ts = get_today_user(user=goal.user)
            goal_completeness = GoalCompleteness.get_one_by(goal=goal,
                                                            start_ts=start_ts)
            goal_serialized = goal.serialize_flat()
            if goal_completeness:
                goal_serialized['goal_completeness'] = goal_completeness.serialize_flat()
            else:
                goal_serialized['goal_completeness'] = {'total_minutes': 0,
                                                        'value': 0}
            user = User.get_one_by(id=gg_user.user_id)
            gg_user_serialized = gg_user.serialize_flat()
            gg_user_serialized['goal'] = goal_serialized
            user_serialized = user.serialize_flat()
            user_serialized['gravatar_url'] = gravatar(user.email)
            gg_user_serialized['user'] = user_serialized
            gg['users'].append(gg_user_serialized)
        return jsonify(gg)


MODELS_REGISTRY.update({
    'task': {
        'vm': TaskModelView,
        'model': Task,
    },
    'calendar_event': {
        'vm': CalendarEventModelView,
        'model': CalendarEvent
    },
    'activity': {
        'vm': ActivityModelView,
        'model': Activity,
    },
    'activity_session': {
        'vm': ActivitySessionModelView,
        'model': ActivitySession,
        'form_include': ['activity_id'],
    },
    'inbox_entry': {
        'vm': InboxEntryModelView,
        'model': InboxEntry,
        'form_include': ['task_id', 'calendar_event_id', 'activity_session_id', ],
    },
    'goal': {
        'vm': GoalModelView,
        'model': Goal,
        'form_include': ['activity_id'],

    },
    'goal_completeness': {
        'vm': ProddayModelView,
        'model': GoalCompleteness,
        'form_include': ['goal_id'],

    },
    'note': {
        'vm': ProddayModelView,
        'model': Note,
    },
    'notification': {
        'vm': ProddayModelView,
        'model': Notification,
    },
    'tracker': {
        'vm': ProddayModelView,
        'model': Tracker,
    },
    'desktop_app_tracker_session': {
        'vm': TrackerSessionModelView,
        'model': DesktopAppTrackerSession,
    },
    'domain_tracker_session': {
        'vm': TrackerSessionModelView,
        'model': DomainTrackerSession,
    },
    'prodday_app': {
        'vm': ProddayAppModelView,
        'model': ProddayApp,
        'creatable': False,
        'editable': False,
        'deletable': False,
    },
    'component': {
        'vm': ComponentModelView,
        'model': Component,
        'user_needed': False,
        'creatable': False,
        'editable': False,
        'deletable': False,
    },
    'user_view': {
        'vm': ProddayModelView,
        'model': UserView,
    },
    'user_component': {
        'vm': UserComponentModelView,
        'model': UserComponent,
        'form_include': ['component_id'],
    },
    'group_goal': {
        'vm': GroupGoalModelView,
        'model': GroupGoal,
    },
    'group_goal_user_settings': {
        'vm': ProddayModelView,
        'model': GroupGoalUserSettings,
    },
    'group_goal_post': {
        'vm': ProddayModelView,
        'model': GroupGoalPost,
    },
    'group_goal_post_comment': {
        'vm': ProddayModelView,
        'model': GroupGoalPostComment,
    },
    'group_goal_post_reaction': {
        'vm': ProddayModelView,
        'model': GroupGoalPostReaction,
    },
})

# Populate MODELS_REGISTRY with extra metadata
for model_name, registry in MODELS_REGISTRY.iteritems():
    MODELS_REGISTRY[model_name]['name'] = model_name
    VMClass = MODELS_REGISTRY[model_name]['vm']
    vm = VMClass(
        name=model_name,
        model=registry['model'],
        form_include=registry.get('form_include', []),
        form_exclude=registry.get('form_exclude', []),
        form_only=registry.get('form_only', []),
        creatable=registry.get('creatable', True),
        editable=registry.get('editable', True),
        deletable=registry.get('deletable', True),
        user_needed=registry.get('user_needed', True),
    )
    MODELS_REGISTRY[model_name]['vm'] = vm
    vm.register(api)
