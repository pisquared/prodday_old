from copy import copy
from operator import or_

from datetime import timedelta
from flask import render_template
from sqlalchemy import and_

from shipka.store.database import db, AwareDateTime
from shipka.store.database import push as db_push
from shipka.webapp.util import ModelView, get_now_user, get_user
from shipka.webapp.util import get_today_user, get_tomorrow_user
from shipka.webapp.registries import MODELS_REGISTRY


def build_user_ctx(user):
    from webapp.api.models import UserView, GroupGoalUserSettings, Settings, Context
    user_settings = Settings.get_one_by(user=user)
    if not user_settings:
        Settings.create(user=user)
        db_push()
    user_views = UserView.query.filter_by(user=user).order_by(UserView.order).all()
    group_goals_users = GroupGoalUserSettings.get_all_by(user=user)
    group_goals = [ggus.group_goal for ggus in group_goals_users]
    edit_templates = [
        'note.html',
        'task.html',
        'user_component.html',
        'user_view.html',
        'goal.html',
        'activity.html',
        'activity_session.html',
    ]
    ctx = {
        'model_scripts': [
            'notification.js',
        ],
        'user_views': user_views,
        'group_goals': group_goals,
        'user': user,
        'user_settings': user_settings,
        'edit_templates': edit_templates,
        'models': {
            model_view['vm'].name: {
                'name': model_view['vm'].name,
                'name_plural': model_view['vm'].name_plural,
                'name_human': model_view['vm'].name_human,
                'model_constants': model_view['vm'].model_constants,
                'elements': model_view['vm'].build_model_elements(),
            } for model_view in MODELS_REGISTRY.values()
        }
    }
    if user_settings.contexts_enabled:
        ctx['contexts'] = Context.get_all_by()
        ctx['context'] = user.current_context
    if user_settings.notifications_enabled:
        ctx['notification_templates'] = [
                                            'calendar_event',
                                        ],
    return ctx


class ProddayModelView(ModelView):
    def build_model_elements(self):
        elements = self.form_include_map
        for k, w in elements.iteritems():
            elements[k]['effective_type'] = w['type']
        if elements.get('title'):
            elements['title']['effective_type'] = 'title'
        if elements.get('body'):
            elements['body']['effective_type'] = 'body'
        if elements.get('name'):
            elements['name']['effective_type'] = 'title'
        return elements

    def additional_filters(self):
        filters = []
        user = get_user()
        # if hasattr(self.model, 'start_ts'):
        #     start_ts = get_today_user(user)
        #     filters += [self.model.start_ts == start_ts]
        # if hasattr(self.model, 'end_ts'):
        #     end_ts = get_tomorrow_user(user)
        #     filters += [self.model.start_ts == end_ts]
        if hasattr(self.model, 'context') and user.current_context:
            context = user.current_context
            filters += [self.model.context == context]
        return filters


class SinceModelView(ProddayModelView):
    def handle_get_filters(self, filters):
        """override if needed - need to filtered elements"""
        user = get_user()
        since = filters.get('since')
        if since and since.isdigit():
            since = int(since)
            if since in range(1, 24 * 60):
                start_ts = get_now_user(user) - timedelta(minutes=since)
            else:
                start_ts = get_now_user(user) - timedelta(minutes=15)
        else:
            # default - 15 minutes
            start_ts = get_now_user(user) - timedelta(minutes=15)
        return self.model.query.filter(self.model.user == user,
                                       or_(self.model.start_ts > start_ts,
                                           self.model.end_ts == None))


class IntervalModel(object):
    start_ts = db.Column(AwareDateTime())
    end_ts = db.Column(AwareDateTime())

    @classmethod
    def get_filter_continuous_in_bounds(cls, start, end):
        """The interval may be continuing but we are interested
        in intervals that are within bounds"""
        return or_(
            # completely within start-end
            and_(cls.start_ts >= start,
                 cls.end_ts <= end),
            or_(
                # started sometime ago, hasn't ended
                cls.end_ts == None,
                # ended sometime in bounds
                and_(
                    cls.end_ts <= end,
                    cls.end_ts >= start
                )))

    @classmethod
    def get_filter_in_bounds(cls, start, end):
        return or_(
            # completely within start-end
            and_(cls.start_ts >= start,
                 cls.end_ts <= end),
            or_(
                # starts before today, ends after today
                and_(cls.start_ts <= start,
                     cls.end_ts >= end),
                or_(
                    # starts today
                    and_(cls.start_ts >= start,
                         cls.start_ts <= end),
                    # ends today
                    and_(cls.end_ts <= end,
                         cls.end_ts >= start))))


class RecurringModel(IntervalModel):
    RECURRING_DAILY = "DAILY"
    RECURRING_WEEKLY = "WEEKLY"
    RECURRING_MONTHLY = "MONTHLY"
    RECURRING_YEARLY = "YEARLY"

    RECURRING_ENDS_NEVER = "NEVER"
    RECURRING_ENDS_AFTER_N = "AFTER_N"
    RECURRING_ENDS_ON = "ON"

    recurring_start_ts = db.Column(AwareDateTime())
    recurring_end_ts = db.Column(AwareDateTime())
    recurring_type = db.Column(db.String)  # DAILY|WEEKLY|MONTHLY|YEARLY
    recurring_every = db.Column(db.Integer, default=1)  # e.g. every 2nd day, 2nd week etc
    recurring_weekly_mask = db.Column(db.String)  # MTWTFSS -> 1111100(every week day)
    recurring_ends_type = db.Column(db.String, default=RECURRING_ENDS_NEVER)  # NEVER|AFTER_N|ON
    recurring_ends_after_x = db.Column(db.Integer)
    recurring_ends_on = db.Column(AwareDateTime())

    recurring_ended_on = db.Column(AwareDateTime())  # when the scheduling was stopped
    recurring_off_on = db.Column(db.JSON)  # list of dates when the recurring was manually stopped

    @classmethod
    def filter_recurring(cls, recurring_events, start, end):
        # TODO: Handle recurring_every
        # TODO: Handle ENDS_AFTER_X
        # TODO: Handle recurring_ended_on, off_on
        erv = []
        start_of_start = start.replace(hour=0, minute=0, second=0, microsecond=0)
        end_of_end = (end + timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
        delta_period = end_of_end - start_of_start
        days = []
        for d in range(delta_period.days):
            days.append(start_of_start + timedelta(days=d))
        for day in days:
            today_weekday = day.isoweekday()  # 1 is Monday, 7 is Sunday
            for event in recurring_events:
                should_add = False
                if event.recurring_type == cls.RECURRING_DAILY:
                    should_add = True
                elif event.recurring_type == cls.RECURRING_WEEKLY:
                    weekly_mask = event.recurring_weekly_mask
                    event_weekly_mask = weekly_mask[today_weekday - 1]
                    if event_weekly_mask == '1':
                        should_add = True
                elif event.recurring_type == cls.RECURRING_MONTHLY:
                    if event.recurring_start_ts.day == start.day:
                        should_add = True
                elif event.recurring_type == cls.RECURRING_YEARLY:
                    if event.recurring_start_ts.day == start.day \
                            and event.recurring_start_ts.month == event.recurring_start_ts == start.month:
                        should_add = True
                if should_add:
                    # replace event's start_ts with today's
                    event_copy = copy(event)
                    event_copy.start_ts = event.recurring_start_ts.replace(day=day.day,
                                                                           month=day.month,
                                                                           year=day.year)
                    delta = event_copy.recurring_end_ts - event.recurring_start_ts
                    event_copy.end_ts = event_copy.start_ts + delta
                    erv.append(event_copy)
        return list(set(erv))

    @classmethod
    def get_recurring_events_in_bounds(cls, user, start, end):
        recurring_events = cls.query.filter(
            and_(cls.recurring_type != None,  # don't use "is/is not" - it doesn't work
                 and_(cls.user == user,
                      and_(cls.deleted == False,
                           or_(cls.recurring_ends_type == cls.RECURRING_ENDS_NEVER,
                               or_(cls.recurring_ends_type == cls.RECURRING_ENDS_AFTER_N,
                                   and_(
                                       cls.recurring_ends_type == cls.RECURRING_ENDS_ON,
                                       cls.recurring_ends_on >= start
                                   ))))))).all()
        recurring_events = cls.filter_recurring(recurring_events, start, end)
        return recurring_events

    @classmethod
    def get_normal_events_in_bounds(cls, user, start, end):
        regular_events = cls.query.filter(
            and_(cls.recurring_type == None,  # don't use "is/is not" - it doesn't work
                 and_(cls.user == user,
                      and_(cls.deleted == False,
                           cls.get_filter_in_bounds(start, end))))).all()
        return regular_events

    @classmethod
    def get_events_in_bounds(cls, user, start, end):
        normal_events = cls.get_normal_events_in_bounds(user, start, end)
        recurring_events = cls.get_recurring_events_in_bounds(user, start, end)
        return list(set(normal_events + recurring_events))

    @classmethod
    def get_recurring_events_today(cls, user):
        today = get_today_user(user)
        tomorrow = get_tomorrow_user(user)
        return cls.get_recurring_events_in_bounds(user, today, tomorrow)

    @classmethod
    def get_normal_events_today(cls, user):
        today = get_today_user(user)
        tomorrow = get_tomorrow_user(user)
        return cls.get_normal_events_in_bounds(user, today, tomorrow)

    @classmethod
    def get_events_today(cls, user):
        today = get_today_user(user)
        tomorrow = get_tomorrow_user(user)
        return cls.get_events_in_bounds(user, today, tomorrow)
