
var NotificationModel = AppModel.extend({
  urlRoot: '/api/notifications',

  defaults: {
    'id': null,
    'message_template': null,
    'message_context': null,
    'url': null,
    'schedule_ts': null,
    'is_fired': false,
    'fired_ts': null,
    'is_read': false,
    'read_ts': null
  }
});

var NotificationCollection = AppCollection.extend({
  appName: 'notification',
  url: '/api/notifications',
  model: NotificationModel
});

var notificationCollection = new NotificationCollection();
COLLECTIONS['notification'] = notificationCollection;

var notificationContainerShown = false;
var notifContainerTemplate = _.template($("#fe-template-notification-container").html());

function addToNotificationBell(notifCtx) {
  var notifContainerHtml = notifContainerTemplate(notifCtx);
  $('.notifications-container').prepend(notifContainerHtml);
}

function changeBrowserTitle(notifCtx) {
  // TODO
  document.title = "(" + response.unread + ") Prodday";
  $('#favicon').attr('href', '/static/icons/iwannask32_hl.png');
}

function browserNotification(notifCtx) {
  // Browser/OS level notification
  // options: https://pushjs.org/docs/options.html
  Push.create(notifCtx.textTitle, {
    body: notifCtx.textBody,
    // icon: '',
    link: notifCtx.url
  });
}

function buildNotificationContext(notificationModel) {
  // get the notification templates
  var msgTemplate = notificationModel.get('message_template');
  var templateTextTitle = _.template($("#fe-template-notification-text-title-" + msgTemplate).html());
  var templateTextBody = _.template($("#fe-template-notification-text-body-" + msgTemplate).html());

  // get the relevant model from context
  var messageContext = notificationModel.get('message_context');
  var messageModelName = messageContext['model'];
  var messageInstanceId = messageContext['instance_id'];
  var messageCollection = COLLECTIONS[messageModelName.toSnakeCase()];
  var messageModel = messageCollection.get(messageInstanceId);

  // Build title and body
  var textTitle, textBody;
  if (messageModel) {
    textTitle = templateTextTitle({data: messageModel.toJSON()}).trim();
    textBody = templateTextBody({data: messageModel.toJSON()}).trim();
  }
  return {
    title: textTitle,
    body: textBody,
    is_read: notificationModel.get('is_read'),
    url: notificationModel.get('url')
  }
}

function setUnreadBadge(notificationCollection) {
  var unreadNotif = notificationCollection.where({is_read: false});
  var unreadCnt = unreadNotif.length;
  if (unreadCnt !== 0)
    $('.notifications-count').show().text(unreadNotif.length);
  else
    $('.notifications-count').hide().text(0);
}


notificationCollection.on('sync', function () {
  this.each(function (notificationModel) {
    // Build Notification context

    // If it's in the future...
    var schedule_ts = notificationModel.get('schedule_ts');
    var shouldSchedule = moment(schedule_ts).isAfter(moment());
    // console.log('>>> Should I schedule at ' + schedule_ts + ' - ' + shouldSchedule);
    if (shouldSchedule && !_.contains(NOTIFICATION_SCHEDULE, notificationModel.get('id'))) {
      var notifCtx = buildNotificationContext(notificationModel);
      // ... schedule a notification
      NOTIFICATION_SCHEDULE.push(notificationModel.get('id'));
      if (DEBUG)
        console.log('>>> Scheduling at ' + schedule_ts);
      at(schedule_ts, function () {
        browserNotification(notifCtx);
        renderNotificationContainer();
        addToNotificationBell(notifCtx);
        // changeBrowserTitle(notifCtx);
      });
    }
    setUnreadBadge(notificationCollection);
  });
});

function renderNotificationContainer() {
  // TODO: read notifications, dismissed notifications
  if (!notificationContainerShown) {
    notificationContainerShown = true;
    $('.notifications-container').html("");
    notificationCollection.each(function (notificationModel) {
      var notifCtx = buildNotificationContext(notificationModel);
      addToNotificationBell({data: notifCtx});
    });
  }
}

$('#navbar-dropdown-bell').on('show.bs.dropdown', renderNotificationContainer);
