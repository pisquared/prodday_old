_.each(COMPONENTS, function(component) {
  var componentDef = COMPONENT_REGISTRY[component.name];

  var ConcreteListView = AppListView;
  if (componentDef.listMethods) {
    ConcreteListView = ConcreteListView.extend(componentDef.listMethods());
  }
  if (componentDef.filterCollection) {
    ConcreteListView = ConcreteListView.extend({filterCollection: componentDef.filterCollection});
  }

  var ElementViewClass = AppElementView;
  if (componentDef.elementMethods) {
    ElementViewClass = ElementViewClass.extend(componentDef.elementMethods());
  }
  if (componentDef.elementEvents){
    ElementViewClass = ElementViewClass.extend({elementEvents: componentDef.elementEvents})
  }
  if (componentDef.elementBeforeRender){
    ElementViewClass = ElementViewClass.extend({beforeRender: componentDef.elementBeforeRender})
  }
  if (componentDef.elementAfterRender){
    ElementViewClass = ElementViewClass.extend({afterRender: componentDef.elementAfterRender})
  }

  new ConcreteListView({
    el: '#' + component.element,
    collection: COLLECTIONS[component.collectionName],
    collectionName: component.collectionName,
    options: component.options,
    name: component.name,
    type: component.type,
    ElementViewClass: ElementViewClass
  });
});
//
//
// var COMPONENT_NAME_PRODDAY_TRACKER_LIST = 'tracker_list';
//
// var TrackerView = AppElementView.extend({
//   template: _.template($("#fe-template-tracker_list-element").html()),
//
//   events: function () {
//     var parentEvents = AppElementView.prototype.events(this);
//     return _.extend(parentEvents, {});
//   },
//
//   render: function () {
//     createListElement(this);
//   }
// });
//
// var TrackerListView = AppListView.extend({
//   template: _.template($("#fe-template-tracker_list-list").html()),
//
//   initialize: function () {
//     this.render();
//   },
//
//   render: function () {
//     var html = this.template();
//     this.$el.html(html);
//     this.$('.trackers-list').html('');
//     this.collection.each(function (model) {
//       var item = new TrackerView({model: model});
//       this.$('.trackers-list').append(item.$el);
//     }.bind(this));
//     return this;
//   }
// });
//
//
// initComponent(COMPONENT_NAME_PRODDAY_TRACKER_LIST, TrackerListView, COLLECTIONS['tracker']);