MODELS.activity.modelMethods = function () {
  return {
    toggleRun: function () {
      this.set('start_ts', new Date());
      this.saveAndPush({is_running: !this.attributes.is_running}, {patch: true});
    }
  }
};

MODELS.task.modelMethods = function () {
  return {
    is_done: function () {
      return this.attributes.status === MODELS.task.constants.STATUS_DONE;
    },

    is_running: function () {
      return this.attributes.status === MODELS.task.constants.STATUS_IN_PROGRESS;
    },

    toggleRun: function () {
      var newStatus = this.is_running() ? MODELS.task.constants.STATUS_NOT_STARTED : MODELS.task.constants.STATUS_IN_PROGRESS;
      this.saveAndPush({status: newStatus}, {patch: true});
    },

    toggleDone: function () {
      var newStatus = this.is_done() ? MODELS.task.constants.STATUS_NOT_STARTED : MODELS.task.constants.STATUS_DONE;
      if (this.is_done())
        this.saveAndPush({status: newStatus}, {patch: true});
      else
        this.saveAndPush({status: newStatus, done_ts: new Date()}, {patch: true});
    }
  }
};

MODELS.task.collectionMethods = function () {
  return {
    comparator: function (model) {
      return -moment(model.get("updated_ts")).unix();
    }
  }
};

MODELS.calendar_event.collectionMethods = function () {
  return {
    comparator: function (model) {
      return -moment(model.get("start_ts")).unix();
    }
  }
};

MODELS.note.collectionMethods = function () {
  return {
    comparator: function (model) {
      return -moment(model.get("updated_ts")).unix();
    }
  }
};


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MAIN REGISTRATION OF MODELS

function registerModels() {
  _.each(MODELS, function (model) {
    var modelObj = {
      urlRoot: '/api/' + model.namePlural,
      defaults: model.defaults
    };

    modelObj = _.extend(modelObj, model.modelMethods());
    var ConcreteModel = AppModel.extend(modelObj);

    var collectionObj = {
      appName: model.name,
      url: '/api/' + model.namePlural,
      model: ConcreteModel
    };

    collectionObj = _.extend(collectionObj, model.collectionMethods());
    var ConcreteCollection = AppCollection.extend(collectionObj);

    COLLECTIONS[model.name] = new ConcreteCollection();
  })
}

registerModels();

_.each(COLLECTIONS, function (value, key) {
  ApplicationRegistry.register(key, {
    'collection': value
  });
});
