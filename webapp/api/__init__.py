from flask import Blueprint

api = Blueprint('api', __name__,
                static_folder='js_models',
                static_url_path='/static/models',
                template_folder='js_templates'
                )

from . import model_views