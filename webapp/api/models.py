import os
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy_utils import ChoiceType

from shipka.store.database import db, User, ModelController, Datable, Deletable, AwareDateTime, Ownable
from shipka.store.search import whooshee
from webapp.api.common import IntervalModel, RecurringModel


class Context(db.Model, ModelController, Datable, Deletable):
    name = db.Column(db.Unicode)
    color = db.Column(db.String)
    fg_color = db.Column(db.String)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", foreign_keys=[user_id])

    def __repr__(self):
        return '<Context %r: %r>' % (self.id, self.name)


# Modify User dynamically
User.current_context_id = db.Column(db.Integer, db.ForeignKey('context.id'))
User.current_context = db.relationship("Context", foreign_keys=[User.current_context_id])


class Contextable(object):
    @declared_attr
    def context_id(self):
        return db.Column(db.Integer, db.ForeignKey('context.id'))

    @declared_attr
    def context(self):
        return db.relationship("Context")


@whooshee.register_model('name')
class Project(db.Model, ModelController, Datable, Ownable, Deletable):
    name = db.Column(db.String)

    parent_project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    parent_project = db.relationship("Project", remote_side='Project.id', backref='subprojects')

    def __repr__(self):
        return '<Project %r: %r %r>' % (self.id, self.name, self.user)


@whooshee.register_model('name')
class TaskList(db.Model, ModelController, Datable, Ownable, Deletable):
    name = db.Column(db.String)

    project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    project = db.relationship("Project", backref='task_lists')

    def __repr__(self):
        return '<Project %r: %r %r>' % (self.id, self.name, self.user)


@whooshee.register_model('body')
class Task(db.Model, ModelController, Datable, Ownable, Deletable):
    STATUS_NOT_STARTED = u"NOT_STARTED"
    STATUS_IN_PROGRESS = u"IN_PROGRESS"
    STATUS_DONE = u"DONE"

    STATUSES = [
        (STATUS_NOT_STARTED, 'not started',),
        (STATUS_IN_PROGRESS, 'in progress',),
        (STATUS_DONE, 'done',),
    ]

    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)

    status = db.Column(ChoiceType(STATUSES), default=STATUS_NOT_STARTED)

    in_progress_ts = db.Column(AwareDateTime())
    done_ts = db.Column(AwareDateTime())

    start_ts = db.Column(AwareDateTime())
    due_ts = db.Column(AwareDateTime())

    task_list_id = db.Column(db.Integer, db.ForeignKey('task_list.id'))
    task_list = db.relationship("TaskList", backref='tasks')

    @property
    def is_done(self):
        return self.status == self.STATUS_DONE

    @property
    def is_running(self):
        return self.status == self.STATUS_IN_PROGRESS

    def __repr__(self):
        return '<Task %r: %r %r %r>' % (self.id, self.body, self.user, self.status)


@whooshee.register_model('name')
class Calendar(db.Model, ModelController, Datable, Ownable, Deletable):
    name = db.Column(db.Unicode)

    project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    project = db.relationship("Project", backref='calendars')

    def __repr__(self):
        return '<Calendar %r: %r %r>' % (self.id, self.name, self.user)


@whooshee.register_model('body', 'title')
class CalendarEvent(db.Model, ModelController, Datable, Deletable, Ownable, RecurringModel):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)

    calendar_id = db.Column(db.Integer, db.ForeignKey('calendar.id'))
    calendar = db.relationship("Calendar", backref='calendar_events')

    def __repr__(self):
        return '<CalendarEvent %r: %r %r %r-%r>' % (self.id, self.body, self.user, self.start_ts, self.end_ts)


class Reminder(db.Model, ModelController, Datable, Deletable, Ownable, RecurringModel):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)

    def __repr__(self):
        return '<Reminder %r - %r>' % (self.id, self.title)


@whooshee.register_model('name')
class Journal(db.Model, ModelController, Datable, Ownable, Deletable):
    name = db.Column(db.Unicode)

    project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    project = db.relationship("Project", backref='journals')

    def __repr__(self):
        return '<Journal %r: %r %r>' % (self.id, self.name, self.user)


@whooshee.register_model('body', 'title')
class JournalEntry(db.Model, ModelController, Datable, Ownable, Deletable):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)

    journal_id = db.Column(db.Integer, db.ForeignKey('journal.id'))
    journal = db.relationship("Journal", backref='journal_entries')

    def __repr__(self):
        return '<JournalEntry %r: %r>' % (self.id, self.journal)


@whooshee.register_model('name')
class Notebook(db.Model, ModelController, Datable, Ownable, Deletable):
    name = db.Column(db.String)

    project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    project = db.relationship("Project", backref='notebooks')

    def __repr__(self):
        return '<Notebook %r: %r %r>' % (self.id, self.name, self.user)


@whooshee.register_model('body', 'title')
class Note(db.Model, ModelController, Datable, Ownable, Deletable):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)

    notebook_id = db.Column(db.Integer, db.ForeignKey('notebook.id'))
    notebook = db.relationship("Notebook", backref='notes')

    def __repr__(self):
        return '<Note %r: %r>' % (self.id, self.user)


@whooshee.register_model('name')
class Checklist(db.Model, ModelController, Datable, Ownable, Deletable):
    name = db.Column(db.String)

    project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    project = db.relationship("Project", backref='checklists')

    def __repr__(self):
        return '<Checklist %r: %r %r>' % (self.id, self.name, self.user)


@whooshee.register_model('body', 'title')
class ChecklistItem(db.Model, ModelController, Datable, Ownable, Deletable):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)

    checklist_id = db.Column(db.Integer, db.ForeignKey('checklist.id'))
    checklist = db.relationship("Checklist", backref='cheklist_items')

    def __repr__(self):
        return '<ChecklistItem %r: %r>' % (self.id, self.user)


@whooshee.register_model('name')
class NumberSeries(db.Model, ModelController, Datable, Ownable, Deletable):
    name = db.Column(db.String)

    project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    project = db.relationship("Project", backref='number_series')

    def __repr__(self):
        return '<NumberSeries %r: %r %r>' % (self.id, self.name, self.user)


@whooshee.register_model('body', 'title')
class Number(db.Model, ModelController, Datable, Ownable, Deletable):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)

    value = db.Column(db.Integer)
    exponent = db.Column(db.Integer, default=0)  # 10^x

    number_series_id = db.Column(db.Integer, db.ForeignKey('number_series.id'))
    number_series = db.relationship("NumberSeries", backref='numbers')

    def __repr__(self):
        return '<Number %r: %r>' % (self.id, self.user)


def ACTIVITY_ICONS():
    activity_icons = []
    for _, _, files in os.walk(os.path.join('webapp', 'api', 'js_models', 'img', 'activity')):
        activity_icons = files
    return [(unicode(activity), unicode(activity)) for activity in activity_icons]


@whooshee.register_model('name')
class Activity(db.Model, ModelController, Datable, Deletable, Ownable):
    name = db.Column(db.String)
    icon = db.Column(ChoiceType(ACTIVITY_ICONS()))

    is_running = db.Column(db.Boolean, default=False)
    start_ts = db.Column(AwareDateTime())
    running_activity_session_id = db.Column(db.Integer)

    active_weight = db.Column(db.Float, default=0.25)

    def __repr__(self):
        return '<Activity %r>' % self.name


class ActivitySession(db.Model, ModelController, Datable, Deletable, IntervalModel, Ownable):
    STATUS_IN_PROGRESS = "IN_PROGRESS"
    STATUS_DONE = "DONE"

    START_TYPE_AUTOMATIC = "AUTOMATIC"
    START_TYPE_MANUAL = "MANUAL"

    status = db.Column(db.String, default=STATUS_IN_PROGRESS)

    start_type = db.Column(db.String, default=START_TYPE_MANUAL)
    comment = db.Column(db.String)

    activity_id = db.Column(db.Integer, db.ForeignKey('activity.id'))
    activity = db.relationship("Activity", backref='sessions')

    def serialize_flat(self, with_=None, depth=0, withs_used=None):
        resp = super(ActivitySession, self).serialize_flat(with_=with_, depth=depth, withs_used=withs_used)
        activity_id = resp.get('activity_id')
        if activity_id:
            activity = Activity.get_one_by(id=activity_id)
            resp['activity'] = activity.serialize_flat()
        return resp

    def __repr__(self):
        return '<ActivitySession %r: %r %r-%r>' % (self.id, self.activity, self.start_ts, self.end_ts)


@whooshee.register_model('name')
class Goal(db.Model, ModelController, Datable, Deletable, Ownable):
    PERIOD_DAY = u"DAY"
    PERIOD_WEEK = u"WEEK"
    PERIOD_FORTNIGHT = u"FORTNIGHT"
    PERIOD_MONTH = u"MONTH"

    LIMIT_MAX = u"MAX"
    LIMIT_MIN = u"MIN"

    PERIODS = [
        (PERIOD_DAY, u'day'),
        (PERIOD_WEEK, u'week'),
        (PERIOD_FORTNIGHT, u'fortnight'),
        (PERIOD_MONTH, u'month'),
    ]

    LIMITS = [
        (LIMIT_MIN, u'least'),
        (LIMIT_MAX, u'most'),
    ]

    name = db.Column(db.String)
    frequency = db.Column(db.Integer, default=1)
    active_minutes = db.Column(db.Integer)
    period = db.Column(ChoiceType(PERIODS))
    limit = db.Column(ChoiceType(LIMITS))

    activity_id = db.Column(db.Integer, db.ForeignKey('activity.id'))
    activity = db.relationship("Activity", backref='goals')

    project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    project = db.relationship("Project", backref='goals')

    def __repr__(self):
        return '<Goal %r: %r %r>' % (self.id, self.name, self.user)


class GoalCompleteness(db.Model, ModelController, Datable, IntervalModel, Ownable):
    goal_id = db.Column(db.Integer, db.ForeignKey('goal.id'))
    goal = db.relationship("Goal", backref='goal_completeness')

    value = db.Column(db.Integer)
    total_minutes = db.Column(db.Integer)

    def __repr__(self):
        return '<GoalCompleteness %r: %r %r>' % (self.id, self.goal, self.value)


class GroupGoal(db.Model, ModelController, Datable, Deletable, Ownable):
    VISIBILITY_PUBLIC = u"PUBLIC"
    VISIBILITY_MEMBERS = u"MEMBERS"

    VISIBILITIES = [
        (VISIBILITY_PUBLIC, 'public',),
        (VISIBILITY_MEMBERS, 'members only',),
    ]

    name = db.Column(db.Unicode)
    body = db.Column(db.Unicode)

    visibility = db.Column(ChoiceType(VISIBILITIES), default=VISIBILITY_MEMBERS)

    user_view_id = db.Column(db.Integer, db.ForeignKey('user_view.id'))
    user_view = db.relationship("UserView", backref='group_goal')

    def __repr__(self):
        return '<GroupGoal %r>' % self.name


class GroupGoalUserSettings(db.Model, ModelController, Datable, Deletable, Ownable):
    ROLE_MEMBER = u"MEMBER"
    ROLE_ADMIN = u"ADMIN"

    ROLES = [
        (ROLE_MEMBER, 'member',),
        (ROLE_ADMIN, 'admin',),
    ]

    user_role = db.Column(ChoiceType(ROLES), default=ROLE_MEMBER)

    group_goal_id = db.Column(db.Integer, db.ForeignKey('group_goal.id'))
    group_goal = db.relationship("GroupGoal", backref='group_goal_user_settings')

    goal_id = db.Column(db.Integer, db.ForeignKey('goal.id'))
    goal = db.relationship("Goal", backref='group_goal_user_settings')

    def __repr__(self):
        return '<GroupGoalUser %r %r>' % (self.group_goal, self.user)


class GroupGoalPost(db.Model, ModelController, Datable, Deletable, Ownable):
    POST_TYPE_MEMBER = u"MEMBER"
    POST_TYPE_AUTO = u"AUTO"

    POST_TYPES = [
        (POST_TYPE_MEMBER, 'member',),
        (POST_TYPE_AUTO, 'auto',),
    ]

    body = db.Column(db.Unicode)
    image_url = db.Column(db.String)
    pinned = db.Column(db.Boolean, default=False)

    post_type = db.Column(ChoiceType(POST_TYPES), default=POST_TYPE_MEMBER)

    group_goal_id = db.Column(db.Integer, db.ForeignKey('group_goal.id'))
    group_goal = db.relationship("GroupGoal", backref='posts')

    activity_session_id = db.Column(db.Integer, db.ForeignKey('activity_session.id'))
    activity_session = db.relationship("ActivitySession", backref='group_goal_posts')

    def __repr__(self):
        return '<GroupGoalPost %r %r>' % (self.group_goal, self.user)


class GroupGoalPostComment(db.Model, ModelController, Datable, Deletable, Ownable):
    body = db.Column(db.Unicode)

    group_goal_post_id = db.Column(db.Integer, db.ForeignKey('group_goal_post.id'))
    group_goal_post = db.relationship("GroupGoalPost", backref='comments')

    def __repr__(self):
        return '<GroupGoalPostComment %r %r>' % (self.group_goal_post, self.user)


class GroupGoalPostReaction(db.Model, ModelController, Datable, Deletable, Ownable):
    REACTION_TYPE_LIKE = u"LIKE"
    REACTION_TYPE_DISLIKE = u"DISLIKE"

    REACTION_TYPES = [
        (REACTION_TYPE_LIKE, 'like',),
        (REACTION_TYPE_DISLIKE, 'dislike',),
    ]

    reaction_type = db.Column(ChoiceType(REACTION_TYPES), default=REACTION_TYPE_LIKE)

    group_goal_post_id = db.Column(db.Integer, db.ForeignKey('group_goal_post.id'))
    group_goal_post = db.relationship("GroupGoalPost", backref='reactions')

    def __repr__(self):
        return '<GroupGoalReaction %r %r>' % (self.group_goal_post, self.user)


class Tracker(db.Model, ModelController, Ownable):
    COMPONENT_CATEGORY_DESKTOP_APP = 'DesktopApp'
    COMPONENT_CATEGORY_DOMAIN = 'Domain'
    COMPONENT_CATEGORY_LOCATION = 'Location'

    COMPONENT_CATEGORIES = [
        (COMPONENT_CATEGORY_DESKTOP_APP, 'Desktop Application Tracker'),
        (COMPONENT_CATEGORY_DOMAIN, 'Domain Tracker'),
        (COMPONENT_CATEGORY_LOCATION, 'Location Tracker'),
    ]

    name = db.Column(db.String)

    api_key = db.Column(db.String)
    component_category = db.Column(ChoiceType(COMPONENT_CATEGORIES))

    def __repr__(self):
        return '<Tracker %r>' % self.name


class TrackerSession(Ownable):
    start_ts = db.Column(AwareDateTime())
    end_ts = db.Column(AwareDateTime())

    meta = db.Column(db.JSON)

    @declared_attr
    def tracker_id(self):
        return db.Column(db.Integer, db.ForeignKey('tracker.id'))

    @declared_attr
    def tracker(self):
        return db.relationship("Tracker")


class DesktopAppTrackerSession(db.Model, ModelController, Datable, Deletable, TrackerSession):
    app_name = db.Column(db.String)
    total_time = db.Column(db.Integer)
    active_time = db.Column(db.Integer)

    def __repr__(self):
        return '<DesktopAppTrackerSession %r>' % self.id


class DomainTrackerSession(db.Model, ModelController, Datable, Deletable, TrackerSession):
    domain = db.Column(db.String)
    total_time = db.Column(db.Integer)
    active_time = db.Column(db.Integer)

    def __repr__(self):
        return '<DomainTrackerSession %r>' % self.id


# class TrackerLocationSession(db.Model, ModelController, Datable, Deletable, GenericTrackerSession):
#     name = db.Column(db.String)
#     latitude = db.Column(db.String)
#     longitude = db.Column(db.String)
#     total_time = db.Column(db.Integer)
#
#     tracker_id = db.Column(db.Integer, db.ForeignKey('tracker.id'))
#     tracker = db.relationship("Tracker", backref=db.backref("location_sessions"))
#
#     user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
#     user = db.relationship("User", backref=db.backref("tracker_location_sessions"))
#
#     def __repr__(self):
#         return '<TrackerLocationSession %r %r>' % (self.name, self.id)

class SummaryStat(IntervalModel, Ownable):
    TYPE_LAST_1_MINUTE = "LAST_1_MINUTE"
    TYPE_LAST_5_MINUTES = "LAST_5_MINUTES"
    TYPE_LAST_15_MINUTES = "LAST_15_MINUTES"
    TYPE_LAST_30_MINUTES = "LAST_30_MINUTES"
    TYPE_LAST_1_HOUR = "LAST_1_HOUR"

    TYPE_TODAY = "TODAY"
    TYPE_THIS_WEEK = "THIS_WEEK"
    TYPE_THIS_MONTH = "THIS_MONTH"

    TYPE_HOURLY = "HOURLY"
    TYPE_DAILY = "DAILY"
    TYPE_WEEKLY = "WEEKLY"
    TYPE_MONTHLY = "MONTHLY"

    stat_type = db.Column(db.String)

    @declared_attr
    def tracker_id(self):
        return db.Column(db.Integer, db.ForeignKey('tracker.id'))

    @declared_attr
    def tracker(self):
        return db.relationship("Tracker")


class DesktopAppSummaryStat(db.Model, ModelController, Datable, Deletable, SummaryStat):
    app_name = db.Column(db.String)
    total_time = db.Column(db.Integer)
    active_time = db.Column(db.Integer)


class DomainSummaryStat(db.Model, ModelController, Datable, Deletable, SummaryStat):
    domain = db.Column(db.String)
    total_time = db.Column(db.Integer)
    active_time = db.Column(db.Integer)


class Trigger(Ownable):
    name = db.Column(db.String)
    weight = db.Column(db.Float, default=1.0)

    @declared_attr
    def activity_id(self):
        return db.Column(db.Integer, db.ForeignKey('activity.id'))

    @declared_attr
    def activity(self):
        return db.relationship("Activity")


class DesktopAppTrigger(db.Model, ModelController, Datable, Deletable, Trigger):
    app_names = db.Column(db.JSON)


class DomainTrigger(db.Model, ModelController, Datable, Deletable, Trigger):
    domains = db.Column(db.JSON)


class ProddayApp(db.Model, ModelController):
    """Represents a prodday application"""
    name = db.Column(db.String)
    description = db.Column(db.String)
    developer = db.Column(db.String)
    icon = db.Column(db.String)

    def __repr__(self):
        return '<ProddayApp %r - %r>' % (self.id, self.name)


class Component(db.Model, ModelController):
    name = db.Column(db.String)
    collection = db.Column(db.String)
    type = db.Column(db.String, default="list")

    app_id = db.Column(db.Integer, db.ForeignKey('prodday_app.id'))
    app = db.relationship("ProddayApp", backref='components')

    def __repr__(self):
        return '<Component %r - %r>' % (self.id, self.name)


class UserApp(db.Model, ModelController, Ownable):
    app_id = db.Column(db.Integer, db.ForeignKey('prodday_app.id'))
    app = db.relationship("ProddayApp", backref='user_apps')

    enabled = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return '<UserApp %r - %r: %r>' % (self.id, self.app, self.enabled)


class UserView(db.Model, ModelController, Ownable, Contextable):
    name = db.Column(db.String)
    icon = db.Column(db.String)

    on_main = db.Column(db.Boolean, default=True)  # should it be on the main menu
    order = db.Column(db.Integer)

    def __repr__(self):
        return '<UserView %r - %r>' % (self.id, self.name)


class UserComponent(db.Model, ModelController, Ownable):
    component_id = db.Column(db.Integer, db.ForeignKey('component.id'))
    component = db.relationship("Component", backref='view_components')

    user_view_id = db.Column(db.Integer, db.ForeignKey('user_view.id'))
    user_view = db.relationship("UserView", backref='view_components')

    order = db.Column(db.Integer)
    columns = db.Column(db.Integer, default=12)
    options = db.Column(db.JSON)


class Notification(db.Model, ModelController, Datable, Deletable):
    message_template = db.Column(db.String)
    message_context = db.Column(db.JSON)

    url = db.Column(db.String)

    schedule_ts = db.Column(AwareDateTime())

    is_fired = db.Column(db.Boolean, default=False)
    fired_ts = db.Column(AwareDateTime())

    is_read = db.Column(db.Boolean, default=False)
    read_ts = db.Column(AwareDateTime())

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref=db.backref("notifications"))

    def __repr__(self):
        return '<Notification %r - %r>' % (self.id, self.message_template)


class CalendarEventNotification(db.Model, ModelController, Datable, Deletable):
    notification_id = db.Column(db.Integer, db.ForeignKey('notification.id'))
    notification = db.relationship("Notification", backref=db.backref("calendar_events_notifications"))

    calendar_event_id = db.Column(db.Integer, db.ForeignKey('calendar_event.id'))
    calendar_event = db.relationship("CalendarEvent", backref=db.backref("notifications_calendar_events"))

    def __repr__(self):
        return '<CalendarEventNotification %r - %r>' % (self.calendar_event, self.notification)


class Settings(db.Model, ModelController, Ownable):
    background_img_url = db.Column(db.String)

    contexts_enabled = db.Column(db.Boolean, default=False)
    search_enabled = db.Column(db.Boolean, default=False)
    notifications_enabled = db.Column(db.Boolean, default=False)


class GCMToken(db.Model, ModelController, Datable, Deletable):
    token = db.Column(db.String)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref=db.backref("gcm_tokens"))

    def __repr__(self):
        return '<GCMToken %r - %r>' % (self.id, self.user)


@whooshee.register_model('body')
class InboxEntry(db.Model, ModelController, Datable, Ownable, Deletable):
    """Represents an inbox entry"""
    body = db.Column(db.Unicode)
    is_processed = db.Column(db.Boolean)
    processed_ts = db.Column(AwareDateTime())

    task_id = db.Column(db.Integer, db.ForeignKey('task.id'))
    task = db.relationship("Task", backref='inbox_entries')

    calendar_event_id = db.Column(db.Integer, db.ForeignKey('calendar_event.id'))
    calendar_event = db.relationship("CalendarEvent", backref='inbox_entries')

    activity_session_id = db.Column(db.Integer, db.ForeignKey('activity_session.id'))
    activity_session = db.relationship("ActivitySession", backref='inbox_entries')
