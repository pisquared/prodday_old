var COLLECTION = COLLECTIONS[COLLECTION_NAME];

var ModelView = AppElementView.extend({
  template: _.template($(FE_TEMPLATE).html()),

  initialize: function () {
    this.listenTo(EVENT_BUS, 'updateAttribute', this.onUpdate);
    this.render();
  },
  updatedAttributes: [],

  events: function () {
    var parentEvents = AppElementView.prototype.events(this);
    return _.extend(parentEvents, {
      "click button.submit": "save",
      "click button.secondary-action": "navigateBack"
    });
  },

  render: function () {
    var html = this.template({data: {}});
    this.$el.append(html);
    this.$('span.action').text('New');
    this.$('button.secondary-action').html('<i class="fa fa-arrow-left"></i>').addClass('btn-secondary');
    _.each(MODELS[COLLECTION_NAME].elements, function (elDetails, elName) {
      if (this.$('.editable-' + elName).length) {
        createEditableElement(this, elName, 'Add ' + elName, elDetails);
      }
    }.bind(this));
    return this
  },

  onUpdate: function (attr) {
    if (this.updatedAttributes.indexOf(attr) === -1)
      this.updatedAttributes.push(attr);
  },

  save: function () {
    var updateAttrs = {};
    _.each(this.updatedAttributes, function (attr) {
      updateAttrs[attr] = this.model.get(attr);
    }.bind(this));
    this.collection.createModel(this.model, updateAttrs);
    window.history.back();
  },

  navigateBack: function() {
    window.history.back();
  }
});

calendarEventView = new ModelView({
  el: '#root',
  collection: COLLECTION,
  model: new COLLECTION.model()
});