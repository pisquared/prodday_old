from flask import render_template, redirect, url_for, flash
from flask import request
from flask_login import current_user, login_required
from flask_socketio import join_room, emit

from shipka.store import database
from shipka.webapp import socketio
from shipka.webapp.registries import MODELS_REGISTRY
from shipka.webapp.views.crudy_integration import build_ctx
from shipka.webapp.util import get_latest, get_now_user, get_user
from webapp.api.common import build_user_ctx
from webapp.api.models import UserView, InboxEntry, GroupGoal, ProddayApp, GroupGoalUserSettings
from webapp.client import client
from webapp.init_user import init_user
from webapp.registries import build_dependencies


@client.route('/')
def get_home():
    user = get_user(create=False)
    if user and user.is_authenticated:
        user_views = UserView.get_all_by(user=user)
        if not user_views:
            user_views = init_user(user)
        sorted_user_views = sorted(user_views, key=lambda x: x.order)
        user_view_id = sorted_user_views[0].id
        return redirect(url_for('client.get_view', user_view_id=user_view_id))
    return render_template("index.html")


@client.route('/views/<int:user_view_id>')
@login_required
def get_view(user_view_id):
    user = get_user()
    user_view = UserView.get_one_by(user=user, id=user_view_id)
    if not user_view:
        flash('Error getting view id {}.'.format(user_view_id))
        return redirect('/')
    ctx = build_ctx()
    ctx.update(build_user_ctx(user))
    model_scripts, view = build_dependencies(ctx['model_scripts'], user_view.view_components)
    ctx['user_view'] = user_view
    ctx['view'] = view
    return render_template('view.html',
                           title="Prod Day - User View",
                           **ctx)


@client.route('/group_goals/<int:group_goal_id>')
@login_required
def get_group_goal(group_goal_id):
    user = get_user()
    group_goal = GroupGoal.get_one_by(id=group_goal_id)
    if not group_goal:
        flash('Error getting group goal id {}'.format(group_goal_id))
        return redirect('/')

    if group_goal.visibility == GroupGoal.VISIBILITY_MEMBERS:
        # If the group goal should be visible to members only
        group_goal_user_settings = GroupGoalUserSettings.get_one_by(group_goal_id=group_goal_id,
                                                                    user_id=user.id)
        if not group_goal_user_settings:
            flash('Error getting group goal id {}'.format(group_goal_id))
            return redirect('/')
    user_view = group_goal.user_view
    ctx = build_ctx()
    ctx.update(build_user_ctx(user))
    model_scripts, view = build_dependencies(ctx['model_scripts'], user_view.view_components)
    ctx['model_scripts'] = model_scripts
    ctx['view'] = view
    return render_template('view.html',
                           group_goal=group_goal,
                           title="Prod Day - Group Goal",
                           **ctx)


@client.route('/search')
@login_required
def get_search_results():
    user = get_user()
    q = request.args.get('q')
    try:
        entries = InboxEntry.query. \
            whooshee_search(q). \
            filter_by(user=current_user).all()
    except ValueError as e:
        entries = []
        flash(e)
    ctx = build_ctx()
    ctx.update(build_user_ctx(user))
    return render_template('search_results.html',
                           q=q,
                           entries=entries,
                           title='Prod Day - Search "%q"'.format(q),
                           **ctx)


@socketio.on('components_syn')
def socket_connect(json):
    payload = {}
    if current_user.is_authenticated:
        tz_offset_minutes = json.get('tz_offset_minutes')
        tz_offset_seconds = -(tz_offset_minutes * 60)
        if current_user.tz_offset_seconds != tz_offset_seconds:
            current_user.tz_offset_seconds = tz_offset_seconds
            database.add(current_user)
            database.push()
        model_updates = {}
        for model_name in MODELS_REGISTRY:
            model_updates[model_name] = get_latest(model_name)
        join_room(current_user.id)
        payload = {'user': {'id': current_user.id},
                   'request_sid': request.sid,
                   'datetime': str(get_now_user(current_user)),
                   'models_updates': model_updates}
    print('< components_syn: ' + str(json))
    print('> components_ack: ' + str(payload))
    emit('components_ack', payload)
