# ProdDay
*All your productivity needs in one place*

Prodday provides you with everything you need to have a productive day, no matter what that means for you. Whether you are a student at school or University, self-employed, working with other people or want to achieve you personal goals - Prodday is for you.

![Prodday Screenshot](https://gitlab.com/pisquared/prodday/raw/master/docs/screenshots/prodday_material2.png)
 
Built upon productivity systems such as [Getting Things Done (GTD)](https://gettingthingsdone.com/), [Objectives and Key Results](https://rework.withgoogle.com/guides/set-goals-with-okrs/steps/introduction/) as well as years of research to narrow down the basic components of productivity systems.

Prodday is extensible on every level. We understand that you may have your own productivity workflow and we want to make it easy for you to set up your own way of working. No matter if you are a professional that has been doing productiviy for years or you are just getting started with organizational skills - give it a go.

Some of the things Prodday gives you:
* **Tasks** - from simple checklists to full project management, get those todo items in one place
* **Calendars** - never forget a meeting, schedule your perfect day or week and let Prodday remind you of where and when you need to be.
* **Goals** - set your own goals or targets and automate tracking them with our [trackers](https://gitlab.com/pisquared/prodday_clients) available for multiple platforms such as Android, Chrome, Linux and more to come. Helps you set [SMART]() goals and makes sure you work on them sheduling and suggesting times that may best work for you.
* **Inbox** - enter everything here - just type **TODO: pick up the laundry** and inbox will put it in your tasks list. Write a **note:**, **event:**, **start activity:** or anything else and process it later.

## Why another productivity tool?
While there are tons of todo, calendar and note-taking apps, tracking, journaling and feel-good apps - they all work in isolation. We believe that putting all of your things in a single productivity system will help you stop looking for the next app that sort-of-kind-of fits your needs. Prodday attempts to combine all that you need and wants to be extendable and configurable enough for professional users so that can fit almost any needs you may have around work and productivity.

Prodday provides you with building blocks and templates to get you started. It's infinitely customizable so that it can fit your particular workflow. It concentrates on storing and syncing the data between devices, displaying your pre-defined views, gives you templates and suggestions, and if something doesn't fit your needs exactly - there is always 3rd party app development.



## Setup

To start working on this project:

1. Make sure you have git installed:
2. Clone this repo:

```
git clone git@gitlab.com:pisquared/prodday.git
cd prodday
```

3. Bootstrap the project:

```
./manage.sh bootstrap
```

This will clone [shipka](https://gitlab.com/pisquared/shipka) repo and do some more initial local setup.

4. Install [vagrant](https://www.vagrantup.com/downloads.html) and [virtualbox](https://www.virtualbox.org/wiki/Downloads) for your OS and then to bring up the virtual machine run:

```
vagrant up
```


After the machine complete provisioning, you can ssh with:

```
vagrant ssh
```

## Check status

Once inside the virtual environment, you can run the `status` command to see the running services:

```
$ ./manage.sh status

monit            Monitoring                 enabled    running    accessible    200  http://localhost:2812
nginx            Reverse proxy              enabled    running    accessible    200  http://localhost:80
eventlet         Local server               enabled    running    accessible    200  http://localhost:5000
postgresql       Database                   enabled    running
pgweb            Database management        enabled    running    accessible    200  http://localhost:8081
celery           Workers                    enabled    running
beat             Scheduling                 enabled    running
flower           Workers management         enabled    running    accessible    200  http://localhost:5555
rabbitmq-server  Queue management           enabled    running    accessible    200  http://localhost:15672
                 Webapp > Queue > Wrk       sent       completed
```

## Run dev

To run development server:

```
$ ./manage.sh runserver --debug-fe=True
```

## Test

To execute the tests, run:

```
$ ./manage.sh test
```


## Prodday architecture
*Prodday* is a modularly build productivity framework. The main application is hosted as a **web server** that can be viewed on any device on [https://prodday.com/](https://prodday.com/). The server collects data from **client trackers**.
 
As an API it provides models, behaviours and abstractions related to productivity.

It is extensible by allowing third party developers. Here are the basic abstractions:

- **Package** - a high level pre-defined templates, views and components for quick startup
- **Component** - a self-contained element (doesn't depend on other elements) that display or interact with underlying Prodday data. A component can be composed with other components into views.
- **View** - end-user defined view of data and interaction elements composed from components.
- **App** - 3rd party developed application that provides **components**, pre-defined **views**, asynchronously executed tasks and scheduled tasks. An app uses the underlying Prodday API to CRUD data.
- [**Client trackers**](https://gitlab.com/pisquared/prodday_clients) - client applications that collect data and send it to the server. They are installed separately on client machines such as an Android Phone, Chrome Browser or Linux desktop 
- **Prodday API** - collection of models, end-user configurations and tasks.
- [**Shipka**](https://gitlab.com/pisquared/shipka) - Prodday itself builts upon the open source DevOps oriented web-framework that provides the underlying infrastructure for Prodday including user management, task management, provisioning and other.
- [**CRUDy**](https://gitlab.com/pisquared/crudy) - Prodday also builts upon a javascript front-end framework for easy interaction with RESTful objects on the backend

![Prodday architecture drawing](https://gitlab.com/pisquared/prodday/raw/master/docs/prodday_architecture.jpg)

### Apps development
To develop apps, put them in the `apps` directory. You need to provide a `manifest.json` file which describes which API elements it uses; and `components` directory. Each component can contain `templates`, `scripts` and `styles` directories which define the component display and behaviour.

### Apps and views Examples
![Prodday API v.1. models, views, settings](https://gitlab.com/pisquared/prodday/raw/master/docs/apiv1_models_views.jpg)

* **Inbox** `[in dev]` - A one input box for everything that gets trickled down to whatever else API depending oncontext. e.g. "Todo:..." creates a task, "Event:..." creates an event etc.

* **Tasks** `[in dev]` - I can add a task with due date.

* **Projects** - I can add a task with due date

* **Calendar** `[in dev]` - I can add events start time, end time, display a calendar of the events in day, week, month view.

* **Activities/Goals** I can create an activity with a usual time and/or location
  - a scheduled activity appears on the calendar
  - if the activity is within time and/or location it appears on the home page as a suggestion
  - I can start tracking the time i spend on an activity
  - the activity appears on the home page if it is currently running
  - I can stop tracking the activity.
    - the activity gets flushed to a log along with the time

* **Journal** I can enter a journal entry.
  - The entry gets displayed nicely formated on the journal page

* **Notes/Lists** I can create notes. I can create lists, check and uncheck items.

![Prodday planner and reporter mocks drawing](https://gitlab.com/pisquared/prodday/raw/master/docs/mock_planner_reporter.jpg)

* **Planner**:
    - calendar daily/weekly planner
    - ‎supports drag and drop of activities and tasks
    - supports nested events (e. g. at work but extra tasks)

* **Reports**:
    - time based reports
    - activity/project based reports

## Powered by [shipka](https://gitlab.com/pisquared/shipka) and [CRUDy](https://gitlab.com/pisquared/crudy)

For more commands, explanation of setup etc, checkout `./manage.sh` with no arguments and the [documentation of shipka](https://gitlab.com/pisquared/shipka/blob/master/README.md).

