# TODO
## Bugs
* [COMPONENTS] BROKEN: IN THE MIDDLE OF ABSTRACTING TO CRUDY - ONLY ACTIVITIES KIND-OF WORK NOW!!!
* [CALENDAR|FE] `$(...).calendar` is not a function
* [CALENDAR|CALENDAR_MONTH] Fetching wrong format of events - the library needs to be preparsed.

## Features for beta
* [ALL_MODELS] Pagination

* [LOCATIONS] Named locations and tracking whether in bounds of location

* [NOTIFICATIONS] Read/unread fired notifications
* [SCHEDULED_NOTIFICATIONS] Notifications that are scheduled in the future
* [NOTIFICATIONS|FE] Front end notifications JS implement + bell + blinking title for important things
* [NOTIFICATIONS|MOBILE] Android implementation with GCM tokens

* [SETTINGS|FE] D&D components to UserViews
* [SETTINGS|FE] Configure UserComponent
* [SETTINGS|FE] Create UserView

* [PACKAGES] School|University|Employed|Self-Employed starter business templates

------ 
## Features for V1

* [ACTIVITY_SESSION] Edit activity session. List them under reports/activity.
* [ACTIVITY] Show top 3 recommended activities now
* [ACTIVITY|FE] Front-end filter of activities
* [CALENDAR|SCHEDULE] "Weekly schedule" calendar with recurring events. Can be overriden day by day.
* [ALGO|ORDERING] Linked list implementation for ordered items on the backend - Drag and drop causes too many updates - see handleReorderUpdates in crudy.js
* [SETTINGS] Register Trackers
* [SETTINGS] Define Triggers
* [INBOX] Autocomplete
* [INBOX] Link entities as extra actions
* [SEARCH] More comprehensive search results
* [REMINDERS]
* [TASK] Connect start of task to start of activity
* [CALENDAR|API] Recurring events - every X days and ends after X days