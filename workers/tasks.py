import re

import datetime
from collections import defaultdict
from sqlalchemy import and_, or_

from shipka.store import database
from shipka.store.database import User
from shipka.webapp.util import get_now_user, global_user, get_beginning_of_week_user
from shipka.workers import celery
from webapp.api.models import Goal, GoalCompleteness, SummaryStat, \
    DesktopAppSummaryStat, DesktopAppTrackerSession, DesktopAppTrigger, \
    DomainSummaryStat, DomainTrackerSession, DomainTrigger, ActivitySession

from webapp.registries import MODELS_REGISTRY

goal_completeness_vm = MODELS_REGISTRY.get('goal_completeness').get('vm')
activity_vm = MODELS_REGISTRY.get('activity').get('vm')

STAT_TYPES = [
    (SummaryStat.TYPE_LAST_1_MINUTE, datetime.timedelta(minutes=1)),
    (SummaryStat.TYPE_LAST_5_MINUTES, datetime.timedelta(minutes=5)),
    (SummaryStat.TYPE_LAST_15_MINUTES, datetime.timedelta(minutes=15)),
    (SummaryStat.TYPE_LAST_1_HOUR, datetime.timedelta(minutes=60)),
]

TRACKER_STAT_TYPES = [
    (SummaryStat.TYPE_LAST_1_MINUTE, datetime.timedelta(minutes=1)),
    (SummaryStat.TYPE_LAST_5_MINUTES, datetime.timedelta(minutes=5)),
]

TRACKER_COMPONENT_REGISTRY = {
    'DesktopApp': {
        'tracker': DesktopAppTrackerSession,
        'tracker_arg': 'app_name',
        'summary_stat': DesktopAppSummaryStat,
        'stat_arg': 'app_name',
        'trigger': DesktopAppTrigger,
        'trigger_arg': 'app_names',
    },
    'Domain': {
        'tracker': DomainTrackerSession,
        'tracker_arg': 'domain',
        'summary_stat': DomainSummaryStat,
        'stat_arg': 'domain',
        'trigger': DomainTrigger,
        'trigger_arg': 'domains',
    }
}


def update_completeness(goal_completeness, goal, intervals):
    # calculate intervals
    total_minutes, value = 0, 0
    for interval in intervals:
        this_interval_minutes = (interval[1] - interval[0]).seconds / 60
        total_minutes += this_interval_minutes
        if this_interval_minutes >= goal.active_minutes:
            value += 1
    # update completeness
    updated_elements = {'value': value,
                        'total_minutes': total_minutes}
    goal_completeness_vm.add_update(goal_completeness, updated_elements)


def process_goals(user):
    goals = Goal.get_all_by(user=user)
    now = get_now_user(user)
    beginning_of_week = get_beginning_of_week_user(user)
    for goal in goals:
        intervals = []
        if goal.period == Goal.PERIOD_WEEK:
            goal_completeness = GoalCompleteness.get_or_create(user=user,
                                                               goal=goal,
                                                               start_ts=beginning_of_week)
            # is the goal_completeness updated before last Activity
            last_updated_activity_session = ActivitySession.query. \
                filter(ActivitySession.user == user,
                       ActivitySession.deleted == False,
                       ActivitySession.activity == goal.activity). \
                order_by(ActivitySession.updated_ts.desc()).first()
            if last_updated_activity_session and \
                    goal_completeness.updated_ts and \
                    last_updated_activity_session.updated_ts and \
                    goal_completeness.updated_ts >= last_updated_activity_session.updated_ts:
                continue
            # we have a newer activity session, update the goal completeness
            activity_sessions = ActivitySession.query.filter(
                ActivitySession.user == user,
                ActivitySession.deleted == False,
                ActivitySession.activity == goal.activity,
                and_(
                    ActivitySession.deleted == False,
                    ActivitySession.get_filter_continuous_in_bounds(
                        start=beginning_of_week,
                        end=now))).all()
            for activity_session in activity_sessions:
                if activity_session.end_ts is None:
                    intervals.append((activity_session.start_ts, now))
                elif activity_session.start_ts >= beginning_of_week and activity_session.end_ts <= now:
                    intervals.append((activity_session.start_ts, activity_session.end_ts))
                elif beginning_of_week <= activity_session.end_ts <= now:
                    intervals.append((beginning_of_week, activity_session.end_ts))
        else:
            # TODO: rest of Goal.PERIODs handling
            continue
        update_completeness(goal_completeness, goal, intervals)


START_THRESHOLD = 0.30


def process_triggers(user):
    rv = {}
    start_activities = []
    running_activities = [session.activity for session in
                          ActivitySession.query.
                              filter(and_(ActivitySession.user == user,
                                          ActivitySession.status == ActivitySession.STATUS_IN_PROGRESS,
                                          ActivitySession.start_type == ActivitySession.START_TYPE_AUTOMATIC)).all()]

    for tracker_component in TRACKER_COMPONENT_REGISTRY.values():
        triggers = tracker_component['trigger'].get_all_by(user=user)
        summary_stats = tracker_component['summary_stat'].get_all_by(user=user,
                                                                     stat_type=SummaryStat.TYPE_LAST_1_MINUTE)
        stats = [getattr(s, tracker_component['stat_arg']) for s in summary_stats]
        for arg in stats:
            for trigger in triggers:
                regex_conditions = getattr(trigger, tracker_component['trigger_arg'])
                for rec in regex_conditions:
                    if re.match(rec, arg):
                        if trigger.activity not in start_activities:
                            start_activities.append(trigger.activity)
                            break

    print('start_activities: ', start_activities)
    print('running_activities: ', running_activities)
    now = get_now_user(user)
    for activity in start_activities:
        if activity.id not in [a.id for a in running_activities]:
            updated_elements = {
                "is_running": True,
                "start_ts": now,
                "start_type": ActivitySession.START_TYPE_AUTOMATIC,
            }
            updated_elements = activity_vm.handle_patch(activity, update=updated_elements)
            activity_vm.add_update(activity, updated_elements)
    for activity in running_activities:
        if activity.id not in [a.id for a in start_activities]:
            updated_elements = {
                "is_running": False,
                "start_ts": None,
                "start_type": ActivitySession.START_TYPE_AUTOMATIC,
            }
            updated_elements = activity_vm.handle_patch(activity, update=updated_elements)
            activity_vm.add_update(activity, updated_elements)
    return [s.serialize_flat() for s in summary_stats]


def get_tracker_sessions(user, tracker_component, start_time):
    tracker_session_model = TRACKER_COMPONENT_REGISTRY.get(tracker_component, {}).get('tracker')
    tracker_sessions = tracker_session_model.query.filter(and_(tracker_session_model.user == user,
                                                               or_(tracker_session_model.start_ts >= start_time,
                                                                   tracker_session_model.end_ts >= start_time))).all()
    return tracker_sessions


def update_stats_for_component(user, tracker_component, stat_type, time_delta):
    end_time = get_now_user(user)
    start_time = end_time - time_delta

    summary_stat_model = TRACKER_COMPONENT_REGISTRY.get(tracker_component, {}).get('summary_stat')

    stats_instances = summary_stat_model.get_all_by(stat_type=stat_type,
                                                    user=user)
    for stats_instance in stats_instances:
        database.delete(stats_instance)
    tracker_sessions = get_tracker_sessions(user, tracker_component, start_time)

    tracker_arg = TRACKER_COMPONENT_REGISTRY.get(tracker_component, {}).get('tracker_arg')
    stat_arg = TRACKER_COMPONENT_REGISTRY.get(tracker_component, {}).get('stat_arg')
    tracker_sessions_hash = defaultdict(list)
    for ts in tracker_sessions:
        tracker_sessions_hash[getattr(ts, tracker_arg)].append(ts)
    stats_hash = {getattr(s, stat_arg): s for s in stats_instances}

    for arg, tracker_sessions in tracker_sessions_hash.iteritems():
        app_total_time = 0
        app_active_time = 0
        stats_instance = stats_hash.get(arg)
        if not stats_instance:
            create_d = {
                'stat_type': stat_type,
                'user': user,
                stat_arg: arg,
            }
            stats_instance = summary_stat_model.create(**create_d)
        for tracker_session in tracker_sessions:
            app_total_time += tracker_session.total_time
            app_active_time += tracker_session.active_time
        stats_instance.total_time = app_total_time
        stats_instance.active_time = app_active_time
        stats_instance.start_ts = start_time
        stats_instance.end_ts = end_time
        database.add(stats_instance)
    database.push()


def process_stats_for_user(user_id):
    user = User.query.filter_by(id=user_id).first()
    global_user.set(user)
    for stat_type, time_delta in STAT_TYPES:
        for tracker_component in TRACKER_COMPONENT_REGISTRY:
            update_stats_for_component(user, tracker_component, stat_type, time_delta)
    # TODO: Redo the below function to start triggers that need to be started
    process_triggers(user)
    process_goals(user)
    global_user.set(None)


@celery.task()
def process_stats():
    users = User.get_all_by(id=1)
    for user in users:
        process_stats_for_user(user_id=user.id)
