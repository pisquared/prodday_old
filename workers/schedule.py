from shipka.workers import celery

celery.conf.beat_schedule = {
    # 'process-stats-every-minute': {
    #     'task': 'workers.tasks.process_stats',
    #     'schedule': 60.0,
    #     'args': (),
    # },
}
celery.conf.timezone = 'UTC'
