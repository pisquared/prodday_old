COMPONENT_REGISTRY.task_list = {
};

//var COMPONENT_NAME_TASKS_LIST = 'task_list';
//
//var TaskView = AppElementView.extend({
//  template: _.template($("#fe-template-task_list-element").html()),
//
//  events: function () {
//    var parentEvents = AppElementView.prototype.events(this);
//    return _.extend(parentEvents, {
//      'click .task-checkbox': 'clickToggleDone',
//      'click .task-start': 'clickRun'
//    });
//  },
//
//  strikeDone: function () {
//    if (this.model.is_done()) {
//      this.$('.entry').addClass("stroken");
//    } else {
//      this.$('.entry').removeClass("stroken");
//    }
//  },
//
//  clickToggleDone: function (event) {
//    this.model.toggleDone();
//    // strike-through done tasks
//    this.strikeDone();
//  },
//
//  clickRun: function (event) {
//    this.model.toggleRun();
//  },
//
//  render: function () {
//    createListElement(this);
//    // strike-through done tasks
//    this.strikeDone();
//  }
//});
//
//var TaskListView = AppListView.extend({
//  template: _.template($("#fe-template-task_list-list").html()),
//  appName: 'task',
//
//  initialize: function (options) {
//    AppListView.prototype.initialize.apply(this, options);
//    this.options = options.options;
//    this.render();
//  },
//
//  render: function () {
//    this.$el.html(this.template({options: this.options}));
//    // filter
//    var collection = this.collection.where({});
//    if (this.options.status)
//      collection = this.collection.where({status: this.options.status});
//
//    collection.length ? this.$el.show() : this.$el.append("<p class='lead'>No tasks</p>");
//    // sort
//    collection.comparator = function (model) {
//      return model.get('due_ts');
//    };
//    this.collection.sort();
//
//    $.each(collection, function (_, model) {
//      var item = new TaskView({model: model});
//      this.$el.append(item.$el);
//    }.bind(this));
//  }
//});
//
//initComponent(COMPONENT_NAME_TASKS_LIST, TaskListView, COLLECTIONS['task']);