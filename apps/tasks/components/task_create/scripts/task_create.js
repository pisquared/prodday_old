var COMPONENT_NAME_TASK_CREATE = 'task_create';

var TaskCreateView = Backbone.View.extend({
  template: _.template($("#fe-template-task_create-input").html()),

  events: {
    'click button[type=submit]': 'onCreate',
    'click textarea': 'enableAutoexpand',
    'focusout textarea': 'shrink',
    'enter textarea': 'onCreate'
  },

  initialize: function () {
    this.render();
    $('textarea').keyup(function (e) {
      if (e.keyCode == 13) {
        $(this).trigger('enter');
      }
    });
  },

  render: function () {
    var html = this.template();
    this.$el.html(html);
    return this;
  },

  enableAutoexpand: function (e) {
    var el = e.target;
    setTimeout(function () {
      el.style.cssText = 'height:auto;';
      el.style.cssText = 'height:' + (el.scrollHeight + 40) + 'px';
    }, 0);
    this.$('.create-aditional').show();
  },

  shrink: function (e) {
    var el = e.target;
    if ($(el).val() === "") {
      el.style.cssText = 'height:auto;';
      this.$('.create-aditional').hide();
    }
  },

  onCreate: function () {
    var attrs = {
      title: this.$('textarea[name=title]').val(),
      start_ts: this.$('#task-start-ts').val(),
      due_ts: this.$('#task-due-ts').val(),
      request_sid: REQUEST_SID
    };
    var model = new this.collection.model();
    this.collection.createModel(model, attrs, {at: 0});
    this.$('textarea[name=title]').val("");
  }
});

initComponent(COMPONENT_NAME_TASK_CREATE, TaskCreateView, COLLECTIONS['task']);

