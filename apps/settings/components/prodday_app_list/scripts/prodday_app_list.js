var COMPONENT_NAME_PRODDAY_APPS_LIST = 'prodday_app_list';

var ProddayAppView = AppElementView.extend({
  template: _.template($("#fe-template-prodday_app_list-element").html()),

  events: function () {
    var parentEvents = AppElementView.prototype.events(this);
    return _.extend(parentEvents, {});
  },

  render: function () {
    createListElement(this);
  }
});

var ProddayAppListView = AppListView.extend({
  template: _.template($("#fe-template-prodday_app_list-list").html()),
  appName: 'prodday_app',

  initialize: function (options) {
    AppListView.prototype.initialize.apply(this, options);
    this.options = options.options;
    this.render();
  },

  render: function () {
    this.$el.html(this.template({options: this.options}));
    var collection = this.collection.where({});
    collection.length ? this.$el.show() : this.$el.append("<p class='lead'>No apps</p>");
    $.each(collection, function (_, model) {
      var item = new ProddayAppView({model: model});
      this.$('.prodday-apps-list').append(item.$el);
    }.bind(this));
  }
});

initComponent(COMPONENT_NAME_PRODDAY_APPS_LIST, ProddayAppListView, COLLECTIONS['prodday_app']);