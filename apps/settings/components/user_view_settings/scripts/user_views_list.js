var COMPONENT_NAME_USER_VIEW_SETTINGS = 'user_view_settings';

var UserComponentView = AppElementView.extend({
  template: _.template($("#fe-template-user_view_settings-user-component-element").html()),

  events: function () {
    var parentEvents = AppElementView.prototype.events(this);
    return _.extend(parentEvents, {});
  },

  render: function () {
    createListElement(this);
  }
});

var UserComponentListView = AppListView.extend({
  initialize: function (options) {
    AppListView.prototype.initialize.apply(this, options);
    this.options = options.options;
    this.listenTo(EVENT_BUS, 'user_view_change', this.render);
    this.render(this.options.user_view_id);
  },

  drake: function () {
    return dragula({
      moves: function (el, container, handle) {
        return handle.classList.contains('drag-drop-handle');
      }
    });
  }(),

  render: function (user_view_id) {
    // TODO: Event on selection is received
    // However because the collection is reset below, filtering it with the new user_view_id
    // will result in a null set.
    // So we need to:
    // 1. Re-fetch the collection (it is local anyway so it's fine.
    //    But we can't use fetch() as it is causing an infinite stack recursion (fetch calls render)
    // 2. Completely refresh this element so that the drag and drop
    //    handler is also bound with the new element.
    //    Probably something to do with removing the $el from the DOM and then attaching a new one.
    this.$el.html("");
    // update comparator function
    this.collection.comparator = function (model) {
      return model.get('order');
    };

    // call the sort method
    this.collection.sort();

    // filter collection
    var collection = this.collection.where({'user_view_id': user_view_id});
    // this.collection.reset(collection);
    this.collection.length ? this.$el.show() : this.$el.html('No components for this view');

    // render subviews
    this.collection.each(function (model) {
      var item = new UserComponentView({model: model});
      this.$el.append(item.$el);
    }.bind(this));

    // drag and drop on views
    if (this.drake.containers.indexOf(this.$el[0]) < 0) {
      this.drake.containers.push(this.$el[0]);
      this.drake.on('drop', function (el, target, source, sibling) {
        var elOrder = $(el).find('.order').data('order');
        var siblingOrder = $(sibling).find('.order').data('order');

        // the last element is null
        if (siblingOrder === undefined)
          siblingOrder = this.collection.length + 1;

        handleReorderUpdates(this.collection, elOrder, siblingOrder);

      }.bind(this));
    }
  }
});


/** ------------------------------------------------------------ */

var UserViewView = AppElementView.extend({
  template: _.template($("#fe-template-user_view_settings-user-view-element").html()),

  initialize: function (options) {
    AppElementView.prototype.initialize.apply(this, options);
    this.options = options.options;
  },

  events: function () {
    var parentEvents = AppElementView.prototype.events(this);
    return _.extend(parentEvents, {
      'click': 'changeViewSelection'
    });
  },

  changeViewSelection: function (e) {
    console.log(this.model.id);
    EVENT_BUS.trigger('user_view_change', this.model.id)
  },

  render: function () {
    createListElement(this);
    createEditableElement(this, '.editable-input-body', 'name');
    createEditableElement(this, '.editable-input-order', 'order');
  }
});

var UserViewListView = AppListView.extend({
  initialize: function (options) {
    AppListView.prototype.initialize.apply(this, options);
    this.options = options.options;
    this.render();
  },

  drake: function () {
    return dragula({
      moves: function (el, container, handle) {
        return handle.classList.contains('drag-drop-handle');
      }
    });
  }(),

  render: function () {
    this.$el.html("");
    // update comparator function
    this.collection.comparator = function (model) {
      return model.get('order');
    };

    // call the sort method
    this.collection.sort();

    // filter collection
    var collection = this.collection.where({on_main: true});
    this.collection.reset(collection);
    this.collection.length ? this.$el.show() : this.$el.hide();

    // render subviews
    this.collection.each(function (model) {
      var item = new UserViewView({model: model, options: this.options});
      this.$el.append(item.$el);
    }.bind(this));

    // drag and drop on views
    if (this.drake.containers.indexOf(this.$el[0]) < 0) {
      this.drake.containers.push(this.$el[0]);
      this.drake.on('drop', function (el, target, source, sibling) {
        var elOrder = $(el).find('.order').data('order');
        var siblingOrder = $(sibling).find('.order').data('order');

        // the last element is null
        if (siblingOrder === undefined)
          siblingOrder = this.collection.length + 1;

        handleReorderUpdates(this.collection, elOrder, siblingOrder);

      }.bind(this));
    }
  }
});

/** ------------------------------------------------------------ */

var ComponentView = AppElementView.extend({
  template: _.template($("#fe-template-user_view_settings-component-element").html()),

  initialize: function (options) {
    AppElementView.prototype.initialize.apply(this, options);
    this.options = options.options;
  },

  events: function () {
    var parentEvents = AppElementView.prototype.events(this);
    return _.extend(parentEvents, {
      'click': 'changeViewSelection'
    });
  },

  changeViewSelection: function (e) {
    console.log(this.model.id);
    EVENT_BUS.trigger('user_view_change', this.model.id)
  },

  render: function () {
    createListElement(this);
    createEditableElement(this, '.editable-input-body', 'name');
    createEditableElement(this, '.editable-input-order', 'order');
  }
});

var ComponentListView = AppListView.extend({
  initialize: function (options) {
    AppListView.prototype.initialize.apply(this, options);
    this.options = options.options;
    this.render();
  },

  drake: function () {
    return dragula({
      moves: function (el, container, handle) {
        return handle.classList.contains('drag-drop-handle');
      }
    });
  }(),

  render: function () {
    this.$el.html("");

    // filter collection
    this.collection.length ? this.$el.show() : this.$el.hide();

    var collection = _.filter(this.collection.models, function(model) {
      return model.get('user_app').enabled === true
    });

    // render subviews
    _.each(collection, function (model) {
      var item = new ComponentView({model: model, options: this.options});
      this.$el.append(item.$el);
    }.bind(this));

    // drag and drop on views
    if (this.drake.containers.indexOf(this.$el[0]) < 0) {
      this.drake.containers.push(this.$el[0]);
      this.drake.on('drop', function (el, target, source, sibling) {
        var elOrder = $(el).find('.order').data('order');
        var siblingOrder = $(sibling).find('.order').data('order');

        // the last element is null
        if (siblingOrder === undefined)
          siblingOrder = this.collection.length + 1;

        handleReorderUpdates(this.collection, elOrder, siblingOrder);

      }.bind(this));
    }
  }
});

/** ******************************** */

var UserViewSettings = Backbone.View.extend({
  template: _.template($("#fe-template-user_view_settings-list").html()),

  initialize: function () {
    this.render();
  },

  render: function () {
    var html = this.template();
    this.$el.html(html);
    var userViewListView = new UserViewListView({
      el: this.$('.user-views'),
      collection: COLLECTIONS['user_view']
    });
    var userComponentListView = new UserComponentListView({
      el: this.$('.view-components'),
      collection: COLLECTIONS['user_component'],
      options: {
        user_view_id: 1
      }
    });
    var componentListView = new ComponentListView({
      el: this.$('.components'),
      collection: COLLECTIONS['component']
    });
    return this;
  }
});

initComponent(COMPONENT_NAME_USER_VIEW_SETTINGS, UserViewSettings);

















