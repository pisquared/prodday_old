var COMPONENT_NAME_PRODDAY_TRACKER_LIST = 'tracker_list';

var TrackerView = AppElementView.extend({
  template: _.template($("#fe-template-tracker_list-element").html()),

  events: function () {
    var parentEvents = AppElementView.prototype.events(this);
    return _.extend(parentEvents, {});
  },

  render: function () {
    createListElement(this);
  }
});

var TrackerListView = AppListView.extend({
  template: _.template($("#fe-template-tracker_list-list").html()),

  initialize: function () {
    this.render();
  },

  render: function () {
    var html = this.template();
    this.$el.html(html);
    this.$('.trackers-list').html('');
    this.collection.each(function(model) {
      var item = new TrackerView({model: model});
      this.$('.trackers-list').append(item.$el);
    }.bind(this));
    return this;
  }
});


initComponent(COMPONENT_NAME_PRODDAY_TRACKER_LIST, TrackerListView, COLLECTIONS['tracker']);