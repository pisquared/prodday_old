COMPONENT_REGISTRY.inbox_entry_create = {
};
//var COMPONENT_NAME_INBOX_ENTRY_CREATE = "inbox_entry_create";
//
//var InboxEntryCreateView = Backbone.View.extend({
//  template: _.template($("#fe-template-inbox_entry_create-input").html()),
//
//  events: {
//    'click button[type=submit]': 'onCreate',
//    'click textarea': 'enableAutoexpand',
//    'enter textarea': 'onCreate'
//  },
//
//  initialize: function () {
//    this.render();
//    triggerEnterOn13(this);
//  },
//
//  render: function () {
//    var html = this.template();
//    this.$el.html(html);
//    $('[data-toggle="tooltip"]').tooltip();
//    return this;
//  },
//
//  enableAutoexpand: function (e) {
//    var el = e.target;
//    setTimeout(function () {
//      el.style.cssText = 'height:auto;';
//      el.style.cssText = 'height:' + (el.scrollHeight + 40) + 'px';
//    }, 0);
//    this.$('.create-aditional').show();
//  },
//
//  shrink: function (e) {
//    var el = e.target;
//    if ($(el).val() === "") {
//      el.style.cssText = 'height:auto;';
//      this.$('.create-aditional').hide();
//    }
//  },
//
//  onCreate: function () {
//    var body = this.$('textarea[name=body]').val();
//    if (!body)
//      return
//    var attrs = {
//      body: body,
//      request_sid: REQUEST_SID
//    };
//    var model = new this.collection.model();
//    this.collection.createModel(model, attrs, {at: 0});
//    this.$('textarea[name=body]').val("");
//  }
//});
//
//initComponent(COMPONENT_NAME_INBOX_ENTRY_CREATE, InboxEntryCreateView, COLLECTIONS['inbox_entry']);