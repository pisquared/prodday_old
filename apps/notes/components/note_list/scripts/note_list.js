var COMPONENT_NAME_NOTES_LIST = 'note_list';

var NoteView = AppElementView.extend({
  template: _.template($("#fe-template-note_list-element").html()),

  events: function () {
    var parentEvents = AppElementView.prototype.events(this);
    return _.extend(parentEvents, {
    });
  },

  _resizeParagraph: function () {
    var paragraphCount = this.$('p').length;
    if (paragraphCount === 1) {
      var pEl = this.$('p');
      var textLen = pEl.text().length;
      if (textLen < 50) {
        var sizing = textLen / 50;
        this.$('p').css('font-size', (4 - sizing * 3) + 'vw')
      }
    }
  },

  render: function () {
    createListElement(this);
    this._resizeParagraph();
  }
});

var NoteListView = AppListView.extend({
  template: _.template($("#fe-template-note_list-list").html()),
  appName: 'note',

  initialize: function (options) {
    AppListView.prototype.initialize.apply(this, options);
    this.options = options.options;
    this.render();
  },

  render: function () {
    this.$el.html(this.template({options: this.options}));
    // filter
    var collection = this.collection.where({});
    collection.length ? this.$el.show() : this.$el.append("<p class='lead'>No notes</p>");
    // sort
    collection.comparator = function (model) {
      return model.get('updated_ts');
    };
    this.collection.sort();

    $.each(collection, function (_, model) {
      var item = new NoteView({model: model});
      this.$('.notes-list').append(item.$el);
    }.bind(this));
  }
});

initComponent(COMPONENT_NAME_NOTES_LIST, NoteListView, COLLECTIONS['note']);
