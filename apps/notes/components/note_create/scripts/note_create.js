var COMPONENT_NAME_NOTE_CREATE = 'note_create';

var NoteCreateView = Backbone.View.extend({
  template: _.template($("#fe-template-note_create-input").html()),

  events: {
    'click .action-create': 'onCreate',
    'click textarea': 'initInput',
    'keyup textarea': 'refreshAutoexpand',
    'enter textarea': 'onCreate'
  },

  initialize: function () {
    this.render();
    $('textarea').keyup(function (e) {
      if (e.keyCode == 13 && (e.shiftKey || e.ctrlKey)) {
        $(this).trigger('enter');
      }
    });
  },

  render: function () {
    var html = this.template();
    this.$el.html(html);
    return this;
  },

  _autoExpand: function (el) {
    setTimeout(function () {
      $(el).css('height', 'auto');
      $(el).css('height', el.scrollHeight + 60 + 'px');
    }, 0);
  },

  initInput: function (e) {
    this.$('.create-aditional').show();
    var el = e.target;
    this._autoExpand(el)
  },

  refreshAutoexpand: function (e) {
    var el = e.target;
    this._autoExpand(el)
  },

  shrink: function () {
    this.$('textarea[name=body]').val('');
    this.$('input[name=title]').val('');
    this.$('textarea[name=body]').css('height', 'auto');
    this.$('.create-aditional').hide()
  },

  onCreate: function () {
    var body = this.$('textarea[name=body]').val();
    var title = this.$('input[name=title]').val();
    if (body.trim() === '' && title.trim() === '') {
      this.shrink();
      return
    }
    var attrs = {
      title: title,
      body: body,
      request_sid: REQUEST_SID
    };
    var model = new this.collection.model();
    this.collection.createModel(model, attrs, {at: 0});
    this.shrink();
  }
});

initComponent(COMPONENT_NAME_NOTE_CREATE, NoteCreateView, COLLECTIONS['note']);

