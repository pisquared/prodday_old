var COMPONENT_NAME_GROUP_GOAL_USERS = 'group_goal_users';

var GroupGoalUserModel = AppModel.extend({
  defaults: {
    'id': null
  }
});

var GroupGoalUserView = AppElementView.extend({
  template: _.template($("#fe-template-group_goal_users-element").html()),

  events: function () {
    var parentEvents = AppElementView.prototype.events(this);
    return _.extend(parentEvents, {});
  },

  progressBar: null,

  makeProgressBar: function (el) {
    this.progressBar = new ProgressBar.Circle(el, {
      strokeWidth: 6,
      easing: 'easeInOut',
      duration: 1400,
      color: '#36a',
      trailColor: '#eee',
      trailWidth: 1,
      svgStyle: null
    });

    // calculate the bar completeness
    var expected = this.model.get('goal').frequency;
    var observed = this.model.get('goal').goal_completeness.value;
    var completeness = 0.0;
    if (expected !== 0) {
      completeness = observed / expected;
    }
    this.progressBar.animate(completeness, {
      to: {color: '#6a3'},
      from: {color: '#36a'},
      step: function (state, circle, attachment) {
        circle.path.setAttribute('stroke', state.color);
      }
    });
  },

  fired: false,
  render: function () {
    createListElement(this);
    var circleEl = this.$('.progress-goal-circle');
    if (!this.progressBar) {
      // HACK: this waits for the browser to finish painting the DOM
      setTimeout(function() {
        this.makeProgressBar(circleEl[0]);
      }.bind(this), 0);
    }
  }
});

var GroupGoalUserListView = AppListView.extend({
  appName: 'group_goal',
  template: _.template($("#fe-template-group_goal_users-list").html()),
  users: [],

  initialize: function (options) {
    AppListView.prototype.initialize.apply(this, options);
    this.collection.get(1).fetch({
        ajaxSync: true,
        success: function (m) {
          this.users = m.get('users');
          this.render()
        }.bind(this)
      }
    );
    this.render();
  },

  render: function () {
    this.$el.html(this.template());
    _.each(this.users, function (user) {
      var item = new GroupGoalUserView({model: new GroupGoalUserModel(user)});
      this.$('.row').append(item.$el);
    }.bind(this));
  }
});


initComponent(COMPONENT_NAME_GROUP_GOAL_USERS, GroupGoalUserListView, COLLECTIONS['group_goal']);
