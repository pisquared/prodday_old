var options = {
  events_source: '/calendar_events',
  view: 'month',
  views: {
    year: {
      slide_events: 0,
      enable: 0
    },
    month: {
      slide_events: 0,
      enable: 1
    },
    week: {
      enable: 0
    },
    day: {
      enable: 0
    }
  },
  tmpl_path: '/static/apps/calendar/calendar_month/scripts/calendar/tmpls/',
  tmpl_cache: false,
  first_day: 1,
  onAfterViewLoad: function (view) {
    $('#calendar-title').text(this.getTitle());
    $('.btn-group button').removeClass('active');
    $('button[data-calendar-view="' + view + '"]').addClass('active');
  },
  classes: {
    months: {
      general: 'label'
    }
  }
};

var calendar = null;

function initCalendar() {
  if (calendar)
    return

  $('#component_2').html(_.template($("#fe-template-calendar_month-calendar_month").html()));
  calendar = $('#calendar-container').calendar(options);
  $('a[data-calendar-nav]').each(function () {
    var $this = $(this);
    $this.click(function () {
      calendar.navigate($this.data('calendar-nav'));
    });
    PREVIOUSLY_SELECTED_DAY_EL = $('.cal-day-today');
  });

  $('a[data-calendar-view]').each(function () {
    var $this = $(this);
    $this.click(function () {
      calendar.view($this.data('calendar-view'));
    });
    PREVIOUSLY_SELECTED_DAY_EL = $('.cal-day-today');
  });


// MY CODE ******************************************************

  var PREVIOUSLY_SELECTED_DAY_EL = $('.cal-day-today');

  function selectDate(e) {
    var date = $(this).children('span').data('date');
    $(PREVIOUSLY_SELECTED_DAY_EL).removeClass('cal-day-today');
    $(this).addClass('cal-day-today');
    PREVIOUSLY_SELECTED_DAY_EL = this;
    ApplicationRegistry.getPeriod(date);
    e.stopPropagation()
  }

  $('.cal-month-day').on('click', selectDate);
  $('*[data-cal-date]').on('click', selectDate);
}

initCalendar();

