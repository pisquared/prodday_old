var COMPONENT_NAME_CALENDAR_EVENT_LIST = 'calendar_event_list';

var CalendarEventView = AppElementView.extend({
  template: _.template($("#fe-template-calendar_event_list-element").html()),

  events: function () {
    var parentEvents = AppElementView.prototype.events(this);
    return _.extend(parentEvents, {});
  },

  clickRun: function (event) {
    this.model.toggleRun();
  },

  render: function () {
    createListElement(this);
  }
});

var CalendarEventListView = AppListView.extend({
  appName: 'calendar_event',

  initialize: function (options) {
    AppListView.prototype.initialize.apply(this, options);
    this.collection.fetch({data: {day:"today"}, ajaxSync: true});
    this.options = options;
  },

  render: function () {
    this.$el.html("");
    if (this.collection.length)
      this.$el.show();
    else
      this.$el.hide();
    this.collection.comparator = function (model) {
      return model.get('start_ts');
    };
    this.collection.each(function(model) {
      var item = new CalendarEventView({model: model});
      this.$el.append(item.$el);
    }.bind(this));

    $('.datetime-time').each(function (_, x) {
      $(x).html(moment(x.innerHTML).format("HH:mm"));
    });
    $('.datetime-date').each(function (_, x) {
      $(x).html(moment(x.innerHTML).format("DD MMMM YYYY"));
    });
  }
});

initComponent(COMPONENT_NAME_CALENDAR_EVENT_LIST, CalendarEventListView, COLLECTIONS['calendar_event']);