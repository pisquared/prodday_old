var COMPONENT_NAME_CALENDAR_EVENT_CREATE = 'calendar_event_create';

var CalendarEventCreateView = Backbone.View.extend({
  template: _.template($("#fe-template-calendar_event_create-input").html()),

  events: {
    'click button[type=submit]': 'onCreate',
    'click textarea': 'enableAutoexpand',
    // 'focusout textarea': 'shrink',
    'enter textarea': 'onCreate'
  },

  initialize: function () {
    this.model = new this.collection.model();
    this.render();
  },

  render: function () {
    var html = this.template();
    this.$el.html(html);
    return this;
  },

  enableAutoexpand: function (e) {
    var el = e.target;
    setTimeout(function () {
      el.style.cssText = 'height:auto;';
      el.style.cssText = 'height:' + (el.scrollHeight + 40) + 'px';
    }, 0);
    this.$('.create-aditional').show();
  },

  shrink: function (e) {
    var el = e.target;
    if ($(el).val() === "") {
      el.style.cssText = 'height:auto;';
      this.$('.create-aditional').hide();
    }
  },

  onCreate: function () {
    var attrs = {
      title: this.$('textarea[name=title]').val(),
      start_ts: $('input.input-start-ts').val(),
      end_ts: $('input.input-end-ts').val(),
      request_sid: REQUEST_SID
    };

    this.collection.createModel(this.model, attrs, {at: 0});
    this.$('textarea[name=title]').val("");
    this.shrink($('textarea[name=title]'));
  }
});

initComponent(COMPONENT_NAME_CALENDAR_EVENT_CREATE, CalendarEventCreateView, COLLECTIONS['calendar_event']);

