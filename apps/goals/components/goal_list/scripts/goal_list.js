COMPONENT_REGISTRY.goal_list = {

    elementMethods: function () {
        return {
            progressBar: null,

            makeProgressBar: function (el) {
                this.progressBar = new ProgressBar.Circle(el, {
                  strokeWidth: 6,
                  easing: 'easeInOut',
                  duration: 1400,
                  color: '#36a',
                  trailColor: '#eee',
                  trailWidth: 1,
                  svgStyle: null
                });

                // calculate the bar completeness
                var expected = this.model.get('frequency');
                var goal_completeness = this.model.get('goal_completeness');
                var observed = 0;
                if (goal_completeness !== undefined)
                  observed = this.model.get('goal_completeness').value;
                var completeness = 0.0;
                if (expected !== 0) {
                  completeness = observed / expected;
                }
                this.progressBar.animate(completeness, {
                  to: {color: '#6a3'},
                  from: {color: '#36a'},
                  step: function (state, circle, attachment) {
                    circle.path.setAttribute('stroke', state.color);
                  }
                });
            }
        }
    },

    elementAfterRender: function () {
        var circleEl = this.$('.progress-goal-circle');
        if (!this.progressBar) {
          // HACK: this waits for the browser to finish painting the DOM
          setTimeout(function () {
            this.makeProgressBar(circleEl[0]);
          }.bind(this), 0);
        }
    }
}

//var COMPONENT_NAME_GOALS_LIST = 'goal_list';
//
//var GoalView = AppElementView.extend({
//  template: _.template($("#fe-template-goal_list-element").html()),
//
//  events: function () {
//    var parentEvents = AppElementView.prototype.events(this);
//    return _.extend(parentEvents, {});
//  },
//
//  progressBar: null,
//
//  makeProgressBar: function (el) {
//    this.progressBar = new ProgressBar.Circle(el, {
//      strokeWidth: 6,
//      easing: 'easeInOut',
//      duration: 1400,
//      color: '#36a',
//      trailColor: '#eee',
//      trailWidth: 1,
//      svgStyle: null
//    });
//
//    // calculate the bar completeness
//    var expected = this.model.get('frequency');
//    var goal_completeness = this.model.get('goal_completeness');
//    var observed = 0;
//    if (goal_completeness !== undefined)
//      observed = this.model.get('goal_completeness').value;
//    var completeness = 0.0;
//    if (expected !== 0) {
//      completeness = observed / expected;
//    }
//    this.progressBar.animate(completeness, {
//      to: {color: '#6a3'},
//      from: {color: '#36a'},
//      step: function (state, circle, attachment) {
//        circle.path.setAttribute('stroke', state.color);
//      }
//    });
//  },
//
//  fired: false,
//
//  render: function () {
//    createListElement(this);
//    var circleEl = this.$('.progress-goal-circle');
//    if (!this.progressBar) {
//      // HACK: this waits for the browser to finish painting the DOM
//      setTimeout(function () {
//        this.makeProgressBar(circleEl[0]);
//      }.bind(this), 0);
//    }
//  }
//});
//
//var GoalListView = AppListView.extend({
//  template: _.template($("#fe-template-goal_list-list").html()),
//  appName: 'goal',
//
//  initialize: function (options) {
//    AppListView.prototype.initialize.apply(this, options);
//    this.options = options.options;
//    this.render();
//  },
//
//  render: function () {
//    this.$el.html(this.template({options: this.options}));
//    var collection = this.collection.where({});
//    collection.length ? this.$el.show() : this.$el.append("<p class='lead'>No goals</p>");
//    $.each(collection, function (_, model) {
//      var item = new GoalView({model: model});
//      this.$('.goals-list').append(item.$el);
//    }.bind(this));
//  }
//});
//
//initComponent(COMPONENT_NAME_GOALS_LIST, GoalListView, COLLECTIONS['goal']);