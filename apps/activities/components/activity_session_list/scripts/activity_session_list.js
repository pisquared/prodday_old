COMPONENT_REGISTRY.activity_session_list = {};
// var COMPONENT_NAME_ACTIVITY_SESSIONS_LIST = 'activity_session_list';
//
// var ActivitySessionView = AppElementView.extend({
//   template: _.template($("#fe-template-activity_session_list-element").html()),
//
//   events: function () {
//     var parentEvents = AppElementView.prototype.events(this);
//     return _.extend(parentEvents, {});
//   },
//
//   render: function () {
//     createListElement(this);
//   }
// });
//
// var ActivitySessionListView = AppListView.extend({
//   template: _.template($("#fe-template-activity_session_list-list").html()),
//   appName: 'activity_session',
//
//   initialize: function (options) {
//     AppListView.prototype.initialize.apply(this, options);
//     this.options = options.options;
//     this.render();
//   },
//
//   render: function () {
//     var collection = this.collection.where({});
//     collection.length ? this.$el.show() : this.$el.append("<p class='lead'>No activities</p>");
//     this.$el.html(this.template({options: this.options}));
//     $.each(collection, function (_, model) {
//       var item = new ActivitySessionView({model: model});
//       this.$('.activity-sessions-list').append(item.$el);
//     }.bind(this));
//     this.collection.comparator = function (model) {
//       return model.get('start_ts');
//     };
//
//     $('.datetime-time').each(function (_, x) {
//       if (x.innerHTML)
//         $(x).html(moment(x.innerHTML).format("HH:mm"));
//       else
//         $(x).html('...')
//     });
//     $('.datetime-date').each(function (_, x) {
//       $(x).html(moment(x.innerHTML).format("DD MMMM YYYY"));
//     });
//   }
// });
//
// initComponent(COMPONENT_NAME_ACTIVITY_SESSIONS_LIST, ActivitySessionListView, COLLECTIONS['activity_session']);