COMPONENT_REGISTRY.activity_list = {
  elementEvents: {
    'click .activity-start': 'clickRun'
  },
  elementMethods: function () {
    return {
      setRunningTimer: function () {
        if (this.model.attributes.is_running) {
          var start_ts = this.model.attributes.start_ts;
          momentStart = moment(start_ts);
          momentEnd = moment(new Date());
          var diffSeconds = momentEnd.diff(momentStart, 'seconds');

          var timer = new Timer();
          timer.start({precision: 'seconds', startValues: {seconds: diffSeconds}});
          this.$('.running-time').html(timer.getTimeValues().toString());
          timer.addEventListener('secondsUpdated', function (e) {
            this.$('.running-time').html(timer.getTimeValues().toString());
          }.bind(this));
        }
      },
      clickRun: function (event) {
        this.model.toggleRun();
      }
    }
  },
  elementAfterRender: function () {
    this.setRunningTimer();
  },
  filterCollection: function () {
    return this.options.is_running === undefined ? this.collection.models : this.collection.where({is_running: this.options.is_running});
  }
};

// var ActivityView = AppElementView.extend({
//   template: _.template($("#fe-template-activity_list-element").html()),
//
//   events: function () {
//     var parentEvents = AppElementView.prototype.events(this);
//     return _.extend(parentEvents, {
//       'click .activity-start': 'clickRun'
//     });
//   },
//
//   setRunningTimer: function () {
//     if (this.model.attributes.is_running) {
//       var start_ts = this.model.attributes.start_ts;
//       momentStart = moment(start_ts);
//       momentEnd = moment(new Date());
//       var diffSeconds = momentEnd.diff(momentStart, 'seconds');
//
//       var timer = new Timer();
//       timer.start({precision: 'seconds', startValues: {seconds: diffSeconds}});
//       this.$('.running-time').html(timer.getTimeValues().toString());
//       timer.addEventListener('secondsUpdated', function (e) {
//         this.$('.running-time').html(timer.getTimeValues().toString());
//       }.bind(this));
//     }
//   },
//
//   clickRun: function (event) {
//     this.model.toggleRun();
//   },
//
//   render: function () {
//     createListElement(this);
//     this.setRunningTimer();
//   }
// });

// var ActivityListView = AppListView.extend({
//   template: _.template($("#fe-template-activity_list-list").html()),
//   appName: 'activity',
//
//   events: function () {
//     var parentEvents = AppListView.prototype.events(this);
//     return _.extend(parentEvents, {
//       'keyup .filter': 'filter'
//     });
//   },
//
//   initialize: function (options) {
//     AppListView.prototype.initialize.apply(this, options);
//     this.options = options.options;
//     this.is_running = this.options.is_running;
//     this.render();
//   },
//
//   filter: function () {
//     var filterValue = this.$('.filter').val();
//     if (filterValue.length > 0) {
//       var filteredCollection = this.collection.filter(function (model) {
//         return model.get('name').toLowerCase().indexOf(filterValue) !== -1;
//       });
//       this.renderSubitems(filteredCollection)
//     } else {
//       this.renderSubitems(this.collection.models);
//     }
//   },
//
//   renderSubitems: function (collection) {
//     this.$('.activities-list').html('');
//     $.each(collection, function (_, model) {
//       var item = new ActivityView({model: model});
//       this.$('.activities-list').append(item.$el);
//     }.bind(this));
//   },
//
//   render: function () {
//     var collection = this.is_running === undefined ? this.collection.models : this.collection.where({is_running: this.is_running});
//     collection.length ? this.$el.show() : this.$('.activities-list').append("<p class='lead'>No activities running</p>");
//     this.$el.html(this.template({options: this.options, itemCount: collection.length}));
//     this.renderSubitems(collection);
//   }
// });
//
// initComponent(COMPONENT_NAME_ACTIVITIES_LIST, ActivityListView, COLLECTIONS['activity']);