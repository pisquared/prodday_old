var COMPONENT_NAME_DAILY_REPORT = 'daily_report';

var SINCE = '10';

var CHARTS = {
  'activity': {
    'url': '/api/activity_sessions?with=activity&since=' + SINCE,
    'column1': 'Activity',
    'column2func': function (element) {
      return element['activity'].name
    }
  },
  'desktop_tracker': {
    'url': '/api/desktop_app_tracker_sessions?since=' + SINCE,
    'column1': 'DesktopTracker',
    'column2func': function (element) {
      return element.app_name
    }
  },
  'domain_tracker': {
    'url': '/api/domain_tracker_sessions?since=' + SINCE,
    'column1': 'DomainTracker',
    'column2func': function (element) {
      return element.domain
    }
  }
}

function renderGraph(elementId, options) {
  google.charts.load('current', {'packages': ['timeline']});
  google.charts.setOnLoadCallback(function () {
    drawChart(elementId)
  });

  function drawChart(elementId) {
    var jsonData = $.ajax({
      url: CHARTS[options.chart_type].url,
      dataType: "json",
      async: false
    }).responseText;

    jsonData = JSON.parse(jsonData);
    if (jsonData.length === 0) {
      $('#timeline-' + elementId).html('No data for selected time period.');
      return
    }
    var container = document.getElementById('timeline-' + elementId);
    var chart = new google.visualization.Timeline(container);
    var dataTable = new google.visualization.DataTable();
    dataTable.addColumn({type: 'string', id: CHARTS[options.chart_type].column1});
    dataTable.addColumn({type: 'string', id: 'Name'});
    dataTable.addColumn({type: 'date', id: 'Start'});
    dataTable.addColumn({type: 'date', id: 'End'});
    var convertedData = [];
    column2func = CHARTS[options.chart_type].column2func;

    for (var i = 0; i < jsonData.length; i++) {
      var start_ts = new Date(jsonData[i].start_ts);
      if (jsonData[i].end_ts == null)
        var end_ts = new Date();
      else
        var end_ts = new Date(jsonData[i].end_ts);
      convertedData.push([
        CHARTS[options.chart_type].column1,
        column2func(jsonData[i]),
        start_ts,
        end_ts
      ]);
    }
    dataTable.addRows(convertedData);
    chart.draw(dataTable);
  }
}

initExternalComponent(COMPONENT_NAME_DAILY_REPORT, "#fe-template-daily_report-timeline", renderGraph);
