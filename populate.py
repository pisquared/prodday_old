#!/usr/bin/env python
# coding=utf-8
from datetime import datetime, timedelta
from flask_security.utils import hash_password

from shipka.store import database
from shipka.webapp.util import get_now_user
from shipka.webapp.util import get_user_tz, get_today_user, get_tomorrow_user, get_beginning_of_week_user
from webapp.api.models import ProddayApp, UserView, UserApp, Component, UserComponent, \
    Context, Settings
from webapp.init_user import create_apps_and_components, init_user, init_goal_user


def populate_contexts(admin_user):
    context_work = Context.get_or_create(name=u'work',
                                         color='#3F51B5',
                                         fg_color='dark')
    context_work.user = admin_user
    context_home = Context.get_or_create(name=u'home',
                                         color='#2196F3',
                                         fg_color='dark')
    context_home.user = admin_user
    context_on_the_move = Context.get_or_create(name=u'on the move',
                                                color='#8BC34A',
                                                fg_color='light')
    context_on_the_move.user = admin_user
    return context_work, context_home, context_on_the_move


def populate_inbox_view(admin_user, now):
    if not ProddayApp.get_one_by(name="Inbox"):
        app_inbox = ProddayApp.create(name="Inbox",
                                      description="Add anything.")
        ua = UserApp.create(app=app_inbox, user=admin_user, enabled=True)
        uv = UserView.create(user=admin_user, name='inbox', icon='inbox',
                             order=1)
        database.add(uv)
        database.add(ua)

        c = Component.create(name='inbox_entry_create', app=app_inbox)
        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=1)
        database.add(uc)

        c = Component.create(name='inbox_entry_list', app=app_inbox)
        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=4, columns=6)
        database.add(uc)

        c = Component.create(name='task_list', app=app_inbox)
        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=2, columns=6,
                                  options={'due_ts': 'today'})
        database.add(uc)


def populate_tasks_view(admin_user, now, context):
    if not ProddayApp.get_one_by(name="Tasks"):
        app_todo = ProddayApp.create(name="Tasks",
                                     description="Add tasks.")
        ua = UserApp.create(app=app_todo, user=admin_user, enabled=True)
        uv = UserView.create(user=admin_user, name='tasks', icon='check-square',
                             context=context, order=2)
        database.add(uv)
        database.add(ua)

        c = Component.create(name='task_create', app=app_todo)
        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=1)
        database.add(uc)

        c = Component.create(name='task_list', app=app_todo)
        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=2, columns=4,
                                  options={'status': 'NOT_STARTED'})
        database.add(uc)

        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=4, columns=4,
                                  options={'status': 'DONE'})
        database.add(uc)


def populate_tasks(admin_user, now):
    from webapp.api.models import Task
    Task.get_or_create(title=u"Do something", user=admin_user)
    Task.get_or_create(title=u"Do something due", user=admin_user, due_ts=now + timedelta(hours=1))
    Task.get_or_create(title=u"This is complete", user=admin_user, done_ts=now - timedelta(hours=1),
                       status=Task.STATUS_DONE)


def populate_calendar_view(admin_user, now):
    if not ProddayApp.get_one_by(name="Calendar"):
        app_event = ProddayApp.create(name="Calendar",
                                      description="Add events.")
        ua = UserApp.create(app=app_event, user=admin_user)
        uv = UserView.create(user=admin_user, name='calendar', icon='calendar',
                             order=3)
        database.add(ua)
        database.add(uv)

        c = Component.create(name='calendar_month', app=app_event)
        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=1)
        database.add(uc)

        c = Component.create(name='calendar_event_create', app=app_event)
        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=1)
        database.add(uc)

        c = Component.create(name='calendar_month', app=app_event)
        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=2, columns=6)
        database.add(uc)

        c = Component.create(name='calendar_event_list', app=app_event)
        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=2, columns=6,
                                  options={'status': 'IN_PROGRESS'})
        database.add(uc)


def populate_calendar_events(admin_user, now):
    from webapp.api.models import CalendarEvent
    today = get_today_user(admin_user)
    yesterday = today - timedelta(days=1)
    tomorrow = get_tomorrow_user(admin_user)

    CalendarEvent.get_or_create(title=u"[YESTERDAY] Completely Yesterday", user=admin_user,
                                start_ts=yesterday + timedelta(hours=8),
                                end_ts=yesterday + timedelta(hours=10))
    CalendarEvent.get_or_create(title=u"[TODAY] Started Yesterday, Completed Today", user=admin_user,
                                start_ts=yesterday + timedelta(hours=22),
                                end_ts=today + timedelta(hours=8))
    CalendarEvent.get_or_create(title=u"[TODAY] Completely Today", user=admin_user,
                                start_ts=today + timedelta(hours=8),
                                end_ts=today + timedelta(hours=10))
    CalendarEvent.get_or_create(title=u"[TODAY] Starts Today, Completes Tomorrow", user=admin_user,
                                start_ts=today + timedelta(hours=22),
                                end_ts=tomorrow + timedelta(hours=8))
    CalendarEvent.get_or_create(title=u"[TOMORROW] Completely Tomorrow", user=admin_user,
                                start_ts=tomorrow + timedelta(hours=8),
                                end_ts=tomorrow + timedelta(hours=10))
    CalendarEvent.get_or_create(title=u"[ALL_DAY] Started Yesterday, Completes Tomorrow", user=admin_user,
                                start_ts=yesterday + timedelta(hours=17),
                                end_ts=tomorrow + timedelta(hours=15))
    CalendarEvent.get_or_create(title=u"[BEFORE] Event before", user=admin_user,
                                start_ts=now - timedelta(hours=2),
                                end_ts=now - timedelta(hours=1))
    CalendarEvent.get_or_create(title=u"[NOW] Event now", user=admin_user,
                                start_ts=now,
                                end_ts=now + timedelta(hours=1))
    CalendarEvent.get_or_create(title=u"[NEXT] Event next", user=admin_user,
                                start_ts=now + timedelta(hours=1),
                                end_ts=now + timedelta(hours=2))
    CalendarEvent.get_or_create(title=u"Rest on weekends", user=admin_user,
                                recurring_start_ts=datetime(2018, 1, 15, 8, 0).replace(tzinfo=get_user_tz(admin_user)),
                                recurring_end_ts=datetime(2018, 1, 15, 22, 0).replace(tzinfo=get_user_tz(admin_user)),
                                recurring_type=CalendarEvent.RECURRING_WEEKLY,
                                recurring_weekly_mask="0000011",
                                recurring_ends_type=CalendarEvent.RECURRING_ENDS_NEVER,
                                )
    CalendarEvent.get_or_create(title=u"Work", user=admin_user,
                                recurring_start_ts=datetime(2018, 1, 15, 9, 0).replace(tzinfo=get_user_tz(admin_user)),
                                recurring_end_ts=datetime(2018, 1, 15, 17, 0).replace(tzinfo=get_user_tz(admin_user)),
                                recurring_type=CalendarEvent.RECURRING_WEEKLY,
                                recurring_weekly_mask="1111100",
                                recurring_ends_type=CalendarEvent.RECURRING_ENDS_ON,
                                recurring_ends_on=datetime(2018, 7, 15).replace(tzinfo=get_user_tz(admin_user)),
                                )
    CalendarEvent.get_or_create(title=u"Sleep", user=admin_user,
                                recurring_start_ts=datetime(2018, 1, 1, 23, 0).replace(tzinfo=get_user_tz(admin_user)),
                                recurring_end_ts=datetime(2018, 1, 2, 7, 0).replace(tzinfo=get_user_tz(admin_user)),
                                recurring_type=CalendarEvent.RECURRING_DAILY,
                                recurring_ends_type=CalendarEvent.RECURRING_ENDS_NEVER
                                )


def populate_reminders_view(admin_user, now):
    if not ProddayApp.get_one_by(name="Reminders"):
        app_event = ProddayApp.create(name="Reminders",
                                      description="Add reminders.")
        ua = UserApp.create(app=app_event, user=admin_user)
        uv = UserView.create(user=admin_user, name='reminders', icon='bell', order=4)
        database.add(ua)
        database.add(uv)


def populate_notes_view(admin_user, now):
    if not ProddayApp.get_one_by(name="Notes"):
        app_event = ProddayApp.create(name="Notes",
                                      description="Add notes.")
        ua = UserApp.create(app=app_event, user=admin_user)
        uv = UserView.create(user=admin_user, name='notes', icon='sticky-note',
                             order=5)
        database.add(ua)
        database.add(uv)

        c = Component.create(name='note_create', app=app_event)
        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=1)
        database.add(uc)

        c = Component.create(name='note_list', app=app_event)
        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=2,
                                  options={})
        database.add(uc)


def populate_checklists_view(admin_user, now):
    if not ProddayApp.get_one_by(name="Checklists"):
        app_event = ProddayApp.create(name="Checklists",
                                      description="Add checklists.")
        ua = UserApp.create(app=app_event, user=admin_user)
        uv = UserView.create(user=admin_user, name='checklists', icon='list',
                             order=6)
        database.add(ua)
        database.add(uv)


def populate_journal_view(admin_user, now):
    if not ProddayApp.get_one_by(name="Journal"):
        app_event = ProddayApp.create(name="Journal",
                                      description="Add journal entries.")
        ua = UserApp.create(app=app_event, user=admin_user)
        uv = UserView.create(user=admin_user, name='journal', icon='book',
                             order=7)
        database.add(ua)
        database.add(uv)


def populate_activities_view(admin_user, now):
    if not ProddayApp.get_one_by(name="Activities"):
        app_activities = ProddayApp.create(name="Activities",
                                           description="Manage activities.")
        ua = UserApp.create(app=app_activities, user=admin_user)
        uv = UserView.create(user=admin_user, name='activities', icon='child',
                             order=8)
        database.add(ua)
        database.add(uv)

        c = Component.create(name='daily_report', app=app_activities)
        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=1,
                                  options={'chart_type': 'activity'})
        database.add(uc)

        c = Component.create(name='activity_list', app=app_activities)
        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=2, columns=12,
                                  options={'is_running': True})
        database.add(uc)

        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=3, columns=8,
                                  options={'is_running': False})
        database.add(uc)

        c = Component.create(name='activity_session_list', app=app_activities)
        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=4, columns=4)
        database.add(uc)

        uv = UserView.create(user=admin_user, name='reports', icon='file',
                             order=10)
        database.add(ua)
        database.add(uv)
        #
        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=2,
                                  options={'chart_type': 'desktop_tracker'})
        database.add(uc)

        uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=3,
                                  options={'chart_type': 'domain_tracker'})
        database.add(uc)


def populate_activities(admin_user, now):
    from webapp.api.models import Activity
    # These can be detected by linux tracker
    programming_activity = Activity.get_or_create(name="Programming", icon="programming.png", user=admin_user)
    entertainment_activity = Activity.get_or_create(name="Entertainment", icon="entertainment.png", user=admin_user,
                                                    active_weight=0.0)

    # TODO: these can be detected by chrome/firefox browser extension tracker
    chat_activity = Activity.get_or_create(name="Chat", icon="chat.png", user=admin_user)
    news_activity = Activity.get_or_create(name="News", icon="news.png", user=admin_user)
    reading_activity = Activity.get_or_create(name="Reading", icon="reading.png", user=admin_user)
    research_activity = Activity.get_or_create(name="Research", icon="research.png", user=admin_user)

    # TODO: These can be detected by android sports activity tracker
    running_activity = Activity.get_or_create(name="Running", icon="running.png", user=admin_user)
    walking_activity = Activity.create(name="Walking", icon="walking.png", user=admin_user)
    biking_activity = Activity.get_or_create(name="Biking", icon="biking.png", user=admin_user)
    commuting_activity = Activity.get_or_create(name="Commuting", icon="commuting.png", user=admin_user)

    # TODO: These can be put on a calendar as regular activities
    sleeping_activity = Activity.get_or_create(name="Sleeping", icon="sleep.png", user=admin_user)
    eating_activity = Activity.get_or_create(name="Eating", icon="eating.png", user=admin_user)
    shower_activity = Activity.get_or_create(name="Shower", icon="shower.png", user=admin_user)
    meditating_activity = Activity.get_or_create(name="Meditating", icon="meditation.png", user=admin_user)

    # TODO: These can be put on a calendar as one-offs
    family_activity = Activity.get_or_create(name="Family", icon="family.png", user=admin_user)
    social_activity = Activity.get_or_create(name="Social", icon="social.png", user=admin_user)
    relationship_activity = Activity.get_or_create(name="Relationship", icon="relationship.png", user=admin_user)

    # TODO: These are somewhere inbetween on the calendar
    shopping_activity = Activity.get_or_create(name="Shopping", icon="shopping.png", user=admin_user)
    cleaning_activity = Activity.get_or_create(name="Cleaning", icon="cleaning.png", user=admin_user)
    washing_dishes_activity = Activity.get_or_create(name="Washing dishes", icon="washing_dishes.png", user=admin_user)

    from webapp.api.models import Tracker, DesktopAppTrigger, DomainTrigger
    home_linux_tracker = Tracker.get_or_create(name="Pi2-Laptop Linux App Tracker",
                                               api_key="fdjsia4rhfidiv6876832",
                                               component_category=Tracker.COMPONENT_CATEGORY_DESKTOP_APP,
                                               user=admin_user)

    home_chrome_tracker = Tracker.get_or_create(name="Pi2-Laptop Chrome Domain Tracker",
                                                api_key="fdsafds8794rfdsfdsafdajflds",
                                                component_category=Tracker.COMPONENT_CATEGORY_DOMAIN,
                                                user=admin_user)

    # TRIGGERS
    desktop_app_programming_trigger = DesktopAppTrigger.get_or_create(user=admin_user,
                                                                      name="Programming Desktop App Trigger",
                                                                      activity=programming_activity)
    desktop_app_programming_trigger.app_names = [
        'java',
        'terminator',
        'atom',
    ]

    domain_programming_trigger = DomainTrigger.get_or_create(user=admin_user,
                                                             name="Programming Domain Trigger",
                                                             activity=programming_activity)
    domain_programming_trigger.domains = [
        'localhost',
        '.*github\.com',
        '.*gitlab\.com',
        '.*stackoverflow\.com',
    ]

    domain_chat_trigger = DomainTrigger.get_or_create(user=admin_user,
                                                      name="Chat Domain Trigger",
                                                      activity=chat_activity)
    domain_chat_trigger.domains = [
        '*.messenger.com',
        '*.facebook.com',
    ]

    domain_news_trigger = DomainTrigger.get_or_create(user=admin_user,
                                                      name="News Domain Trigger",
                                                      activity=news_activity)
    domain_news_trigger.domains = [
        '.*\.?reddit\.com',
        'news\.ycombinator\.com',
        '.*hntoplinks\.com',
        '.*dnes\.bg',
        '.*vesti\.bg',
        '.*offnews\.bg',
        'news\.google\.com',
    ]

    entertainment_app_trigger = DesktopAppTrigger.get_or_create(user=admin_user,
                                                                name="audience -> entertainment",
                                                                activity=entertainment_activity)
    entertainment_app_trigger.app_names = [
        'audience',
        'vlc',
    ]

    entertainment_domain_trigger = DomainTrigger.get_or_create(user=admin_user,
                                                               name="Programming Domain Trigger",

                                                               activity=chat_activity)
    entertainment_domain_trigger.domains = [
        '.*youtube\.com',
        '.*9gag\.com',
    ]

    #### GROUP GOALS
    from shipka.store.database import User
    from webapp.api.models import Goal, GroupGoal, GroupGoalUserSettings, GroupGoalPost, \
        GroupGoalPostComment, ActivitySession, GroupGoalPostReaction, GoalCompleteness

    # create a GROUP_GOAL view
    uv = UserView.create(user=admin_user, name='group_goal', on_main=False)
    database.add(uv)

    app_group_goal = ProddayApp.get_one_by(name="Group Goals")
    if not app_group_goal:
        app_group_goal = ProddayApp.create(name="Group Goals",
                                      description="Add group goals.")
    c = Component.create(name='group_goal_users', app=app_group_goal)
    uc = UserComponent.create(user=admin_user, component=c, user_view=uv, order=1)
    database.add(uc)

    # create a group goal
    group_goal_ex = GroupGoal.get_or_create(name=u"Exercise",
                                            body=u"Get going!",
                                            user=admin_user,
                                            )

    # create a user's goal
    goal_ex = Goal.get_or_create(user=admin_user,
                                 name="Run",
                                 frequency=4,
                                 period=Goal.PERIOD_WEEK,
                                 activity=running_activity,
                                 active_minutes=20,  # 20 minutes
                                 )

    # set user's role and goal in this group
    GroupGoalUserSettings.get_or_create(user=admin_user,
                                        user_role=GroupGoalUserSettings.ROLE_ADMIN,
                                        group_goal=group_goal_ex,
                                        goal=goal_ex,
                                        )

    # create a member user
    member_user = User.get_or_create(email="danieltcv@gmail.com")
    member_user.name = u"Дани",
    member_user.password = hash_password("password")
    member_user.tz_offset_seconds = -3600
    database.add(member_user)

    # create member user activity, goal
    biking_activity_member = Activity.get_or_create(user=member_user,
                                                    name="Biking",
                                                    icon="biking.png",
                                                    )

    goal_ex_member = Goal.get_or_create(user=member_user,
                                        name="Biking",
                                        frequency=2,
                                        period=Goal.PERIOD_WEEK,
                                        activity=biking_activity_member,
                                        active_minutes=10,  # 20 minutes
                                        )

    # set user's role and goal in this group
    GroupGoalUserSettings.get_or_create(user=member_user,
                                        user_role=GroupGoalUserSettings.ROLE_MEMBER,
                                        group_goal=group_goal_ex,
                                        goal=goal_ex_member,
                                        )

    # add a session, goal completeness
    biking_activity_session_member = ActivitySession.get_or_create(user=member_user,
                                                                   status=ActivitySession.STATUS_DONE,
                                                                   activity=biking_activity_member,
                                                                   start_ts=now - timedelta(minutes=30),
                                                                   end_ts=now - timedelta(minutes=10))
    GoalCompleteness.get_or_create(user=member_user,
                                   goal=goal_ex_member,
                                   value=1,
                                   total_minutes=20,
                                   start_ts=get_beginning_of_week_user(member_user),
                                   end_ts=now,
                                   updated_ts=now - timedelta(minutes=5),
                                   )

    # create a group goal post, comment, reaction
    GroupGoalPost.create(user=admin_user,
                         body=u"Get going! :)",
                         group_goal=group_goal_ex,
                         )
    ggp = GroupGoalPost.create(user=member_user,
                               post_type=GroupGoalPost.POST_TYPE_AUTO,
                               group_goal=group_goal_ex,
                               activity_session=biking_activity_session_member,
                               )
    GroupGoalPostComment.create(user=member_user,
                                body=u"YEEEAAAH!!",
                                group_goal_post=ggp)
    GroupGoalPostReaction.create(user=admin_user,
                                 reaction_type=GroupGoalPostReaction.REACTION_TYPE_LIKE,
                                 group_goal_post=ggp,
                                 )


def populate():
    # create apps
    from shipka.store.database import User
    admin_user = User.get_one_by(id=1)
    database.add(admin_user)

    # create_apps_and_components()
    init_user(admin_user)
    init_goal_user(admin_user)

    now = get_now_user(admin_user)

    context_work, context_home, context_on_the_move = populate_contexts(admin_user)
    populate_inbox_view(admin_user, now)
    populate_tasks_view(admin_user, now, context_work)
    populate_tasks(admin_user, now)
    populate_calendar_view(admin_user, now)
    populate_calendar_events(admin_user, now)
    populate_notes_view(admin_user, now)
    populate_reminders_view(admin_user, now)
    populate_checklists_view(admin_user, now)
    populate_journal_view(admin_user, now)
    populate_activities_view(admin_user, now)
    populate_activities(admin_user, now)
